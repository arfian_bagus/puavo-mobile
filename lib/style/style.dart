import 'dart:ui';

import 'package:flutter/material.dart';

class Style {
  double _widthScreen;
  double _heightScreen;
  double _verticalValue;
  double _spaceVertical;
  double _horizontalValue;
  double _spaceVerticalx2;
  Color primaryOrange = Color(0xfab32e);
  Color primaryGreen = Color(0x41786b);
  Color backgroundColor = Color(0xFFF3F3F3);

  Style(Size size) {
    _widthScreen = size.width;
    _heightScreen = size.height;
    _verticalValue = _widthScreen / 33;
    _spaceVertical = _widthScreen / 82.2;
    _horizontalValue = _widthScreen / 16;
    _spaceVerticalx2 = _spaceVertical * 2;
  }

  TextStyle fontStyleH1 = new TextStyle(
      fontSize: 35.0, fontWeight: FontWeight.w700, color: Color(0xFF393939));

  TextStyle fontStyleH2 = new TextStyle(
      fontSize: 30.0, fontWeight: FontWeight.w700, color: Color(0xFF393939));

  TextStyle fontStyleH3 = new TextStyle(
      fontSize: 25.0, fontWeight: FontWeight.w500, color: Color(0xFF393939));

  TextStyle fontStyleH4 = new TextStyle(
      fontSize: 20.0, fontWeight: FontWeight.w400, color: Color(0xFF393939));

  TextStyle fontStyleH5 = new TextStyle(
      fontSize: 20.0, fontWeight: FontWeight.w500, color: Color(0xFF393939));

  TextStyle fontStyleH6 = new TextStyle(
      fontSize: 18.0, fontWeight: FontWeight.w400, color: Color(0xFF393939));

  TextStyle fontStyleH7 = new TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.w400,
    color: Color(0xFF393939),
  );

  TextStyle fontStyleH8 = new TextStyle(
      fontSize: 14.0, fontWeight: FontWeight.w400, color: Color(0xFF393939));

  TextStyle fontStyleH9 = new TextStyle(
      fontSize: 13.0, fontWeight: FontWeight.w400, color: Color(0xFF393939));

  TextStyle fontStyleH10 = new TextStyle(
      fontSize: 12.0, fontWeight: FontWeight.w400, color: Color(0xFF393939));

  TextStyle fontStyleH11 = new TextStyle(
      fontSize: 10.0, fontWeight: FontWeight.w400, color: Color(0xFF393939));

  double widthScreen() {
    return _widthScreen;
  }

  double heightScreen() {
    return _heightScreen;
  }

  double paddingTop() {
    return _verticalValue;
  }

  double spaceVertical() {
    return _spaceVertical;
  }

  double spaceVerticalx2() {
    return _spaceVerticalx2;
  }

  double paddingLeft() {
    return _horizontalValue;
  }

  EdgeInsetsGeometry paddingHorizontal() {
    return new EdgeInsets.symmetric(horizontal: _horizontalValue);
  }

  BoxDecoration ShadowWidgetBorder(
      bool isTransparent, bool haveShadow, Color borderColor) {
    var borderStyle = BorderSide(
        color: borderColor, width: borderColor == Colors.transparent ? 0 : 2);
    return BoxDecoration(
      color: isTransparent ? Colors.transparent : Colors.white,
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.3),
          blurRadius: 5.0, // has the effect of softening the shadow
          spreadRadius: -5.0, // has the effect of extending the shadow
          offset: Offset(
            0.0, // horizontal, move right 10
            5.0, // vertical, move down 10
          ),
        )
      ],
      border: haveShadow
          ? Border(left: borderStyle, right: borderStyle)
          : Border.all(color: Colors.transparent, width: 0),
    );
  }

  Widget widgetVerticalShadow({
    Widget child,
    String title,
    double space,
    bool haveShadow = true,
    bool isTransparent = false,
    Color borderColor = Colors.transparent,
  }) {
    var borderStyle = BorderSide(color: borderColor, width: 2);
    return Container(
      decoration: BoxDecoration(
        border: haveShadow
            ? Border(top: borderStyle)
            : Border.all(
                color: borderColor,
                width: borderColor == Colors.transparent ? 0 : 2,
                style: BorderStyle.solid),
        color: isTransparent ? Colors.transparent : backgroundColor,
      ),
      child: Column(
        children: <Widget>[
          new Container(
            width: widthScreen(),
            decoration: haveShadow
                ? ShadowWidgetBorder(isTransparent, haveShadow, borderColor)
                : BoxDecoration(color: Colors.white),
            child: title == null
                ? child
                : new Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Container(
                        width: widthScreen(),
                        padding: EdgeInsets.only(
                            top: title != null ? paddingTop() : spaceVertical(),
                            bottom: title != null ? paddingTop() : spaceVertical()),
                        color: Colors.orange,
                        child: new Padding(
                          padding: paddingHorizontal(),
                          child: new Text(
                            title != null ? title : "",
                            style: fontStyleH5.copyWith(color: Colors.white),
                          ),
                        ),
                      ),
                      new Container(
                        height: space != null ? space : spaceVerticalx2(),
                      ),
                      new Container(
                        margin: EdgeInsets.only(top: spaceVertical()),
                        child: child,
                      )
                    ],
                  ),
          ),
          Visibility(
            visible: haveShadow,
            child: new Container(
              width: widthScreen(),
              height: paddingTop(),
              decoration: BoxDecoration(border: Border(top: borderStyle)),
            ),
          ),
        ],
      ),
    );
  }

  Widget widgetHorizontalShadow({
    Widget child,
    String title,
    double heightCard,
    bool haveShadow = true,
    bool isTransparent = false,
//    bool isToa=false,
    Widget toolbar,
  }) {
    return Container(
      color: isTransparent ? Colors.transparent : backgroundColor,
      child: Column(
        children: <Widget>[
          new Container(
            padding:
                EdgeInsets.only(top: paddingTop(), bottom: spaceVertical()),
            width: widthScreen(),
            decoration: haveShadow ? ShadowWidget(isTransparent) : null,
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                title == null
                    ? Container()
                    : Padding(
                        padding: paddingHorizontal(),
                        child: new Row(children: [
                          new Text(
                            title != null ? title : "",
                            style: fontStyleH5,
                          ),
                          new Expanded(
                              child: Container(
                                  alignment: Alignment.centerRight,
                                  child: toolbar))
                        ])),
                title == null
                    ? Container()
                    : Container(
                        height: spaceVertical(),
                      ),
                new Container(
                  height: heightCard,
                  child: child,
                )
              ],
            ),
          ),
          Visibility(
            visible: haveShadow,
            child: new Container(
              width: widthScreen(),
              height: paddingTop(),
            ),
          ),
        ],
      ),
    );
  }

  BoxDecoration ShadowWidget(bool isTransparent) {
    return BoxDecoration(
      color: isTransparent ? Colors.transparent : Colors.white,
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.3),
          blurRadius: 5.0, // has the effect of softening the shadow
          spreadRadius: -5.0, // has the effect of extending the shadow
          offset: Offset(
            0.0, // horizontal, move right 10
            5.0, // vertical, move down 10
          ),
        )
      ],
    );
  }
}
