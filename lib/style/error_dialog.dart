import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ErrorDialog {
  final BuildContext context;

  String title;
  String message;

  List<Widget> actions;

  ErrorDialog(this.context,
      {this.title, this.message, this.actions = const []});

  ErrorDialog addAction(Widget action) {
    actions.add(action);
    return this;
  }

  ErrorDialog setTitle(String title) {
    this.title = title;
    return this;
  }

  ErrorDialog setMessage(String message) {
    this.message = message;
    return this;
  }

  show() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text((this.title == null) ? "Whoops" : this.title),
          content: Text(this.message),
//          content: Text("Terjadi kesalahan sistem."),
          actions: <Widget>[
            this.actions.isEmpty
                ? FlatButton(
              // isDefaultAction: true,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Close"),
            )
                : this.actions.map((widget) => widget).toList(),
          ],
        );
      },
    );
  }

  sukses() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text((this.title == null) ? "Opinsys" : this.title),
          content: Text(this.message),
//          content: Text("Terjadi kesalahan sistem."),
          actions: <Widget>[
            this.actions.isEmpty
                ? FlatButton(
              // isDefaultAction: true,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Close"),
            )
                : this.actions.map((widget) => widget).toList(),
          ],
        );
      },
    );
  }

  errorConnection() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text((this.title == null) ? "Whoops" : this.title),
          content: Text("Check your internet connection."),
          actions: <Widget>[
            this.actions.isEmpty
                ? FlatButton(
              // isDefaultAction: true,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Close"),
            )
                : this.actions.map((widget) => widget).toList(),
          ],
        );
      },
    );
  }

  showExitDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: new Text(
              "Are you sure close the application ?"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("No"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Yes"),
              onPressed: () {
                SystemChannels.platform.invokeMethod('SystemNavigator.pop');
              },
            ),
          ],
        );
      },
    );
  }
}
