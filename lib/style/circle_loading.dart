import 'package:flutter/material.dart';

class WidgetCircleLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: new SizedBox(
        height: 50.0,
        width: 50.0,
        child: new CircularProgressIndicator(
          value: null,
          strokeWidth: 3.0,
        ),
      ),
    );
  }
}
