import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puavomobile/models/schoolmodel.dart';
import 'package:puavomobile/services/globals.dart' as globals;
import 'package:puavomobile/services/school_api.dart' as school_api;
import 'package:puavomobile/style/circle_loading.dart';
import 'package:puavomobile/style/error_dialog.dart';
import 'package:puavomobile/style/style.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:puavomobile/services/globals.dart' as globals;

class SchoolPage extends StatefulWidget {
  @override
  _SchoolPageState createState() => _SchoolPageState();
}

class _SchoolPageState extends State<SchoolPage> {
  Style _style;
  bool isLoad;
  ScrollController _controller = new ScrollController();
  SchoolModel schoolData = new SchoolModel();

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    globals.setContext(context);
    isLoad = false;
    refreshData();
    super.initState();
  }

  void onRefresh() async {
    await Future.delayed(Duration(milliseconds: 500));

    refreshData();
    _refreshController.refreshCompleted();
    _refreshController.loadComplete();
  }

  Future<void> refreshData() async {
    setState(() {
      isLoad = false;
      schoolData = globals.schoolData;
    });
//    school_api.getSchool().then((res) {
//      if (res.length > 0) {
//        setState(() {
//          schoolData = res[(res.length - 1)];
//          isLoad = false;
//        });
//      }
//    }).catchError((message) {
//      if (message != "error connection")
//        new ErrorDialog(context).setMessage(message).show();
//
//      setState(() {
//        isLoad = false;
//      });
//    });
  }

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
        body: new Container(
            width: _style.widthScreen(),
            height: _style.heightScreen(),
            child: Stack(children: <Widget>[
              isLoad
                  ? WidgetCircleLoading()
                  : new Container(
                      child: new Container(
//                          color: Colors.white,
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: new SmartRefresher(
                              enablePullDown: true,
                              enablePullUp: true,
                              header: MaterialClassicHeader(),
                              footer: CustomFooter(
                                builder:
                                    (BuildContext context, LoadStatus mode) {
                                  Widget body;
                                  if (mode == LoadStatus.idle) {
                                    body = Container();
                                  } else if (mode == LoadStatus.loading) {
                                    body = CupertinoActivityIndicator();
                                  } else if (mode == LoadStatus.failed) {
                                    body = Container();
                                  } else if (mode == LoadStatus.canLoading) {
                                    body = Container();
                                  } else {
                                    body = Container();
                                  }
                                  return Container(
                                    height: 55.0,
                                    child: Center(child: body),
                                  );
                                },
                              ),
                              controller: _refreshController,
                              onRefresh: onRefresh,
                              child: new CustomScrollView(
                                  controller: _controller,
                                  physics: AlwaysScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  slivers: <Widget>[
                                    SliverList(
                                        delegate: SliverChildListDelegate([
                                      new Container(
                                          child: new Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          _style.widgetVerticalShadow(
                                              borderColor: Colors.white,
                                              child: new Container(
                                                  width: _style.widthScreen(),
                                                  child: new Column(
                                                    children: <Widget>[
                                                      new Center(
                                                          child: new Text(
                                                        schoolData.name,
                                                        style: TextStyle(
                                                            fontWeight: FontWeight.w500,
                                                            fontSize: 25),
                                                      )),
                                                      new Container(
                                                        height: 200,
                                                        margin: EdgeInsets.only(
                                                            top: 10),
                                                        decoration:
                                                            BoxDecoration(
                                                          image: DecorationImage(
                                                              image: CachedNetworkImageProvider(
                                                                  "http://opinsys.fi/wp-content/uploads/2016/10/opinsys-1200px-7.jpg"),
                                                              fit:
                                                                  BoxFit.cover),
                                                        ),
                                                      ),
                                                    ],
                                                  ))),
//                                          new Container(
//                                            color: Colors.orange,
//                                            height:
//                                            _style.spaceVertical() * 2,
//                                          ),
                                          _style.widgetVerticalShadow(
                                              title: "General Information",
                                              space: 5,
                                              child: new Container(
                                                  width: _style.widthScreen(),
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal:
                                                          _style.paddingLeft()),
                                                  child: new Column(
                                                    children: <Widget>[
                                                      Row(children: <Widget>[
                                                        Text("Language", style: _style.fontStyleH7,),
                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text(schoolData.preferred_language, style: _style.fontStyleH7),))
                                                      ],),
                                                      SizedBox(height: 5),
                                                      Row(children: <Widget>[
                                                        Text("Allow Guest", style: _style.fontStyleH7,),
                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text(schoolData.allow_guest.toString(), style: _style.fontStyleH7),))
                                                      ],),
                                                      SizedBox(height: 5),
                                                      Row(children: <Widget>[
                                                        Text("Automatic Image Updates", style: _style.fontStyleH7,),
                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text(schoolData.automatic_image_updates.toString(), style: _style.fontStyleH7),))
                                                      ],),
                                                      SizedBox(height: 5),
                                                      Row(children: <Widget>[
                                                        Text("Personal Device", style: _style.fontStyleH7,),
                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text(schoolData.personal_device.toString(), style: _style.fontStyleH7),))
                                                      ],),
                                                      SizedBox(height: 5),
                                                      Row(children: <Widget>[
                                                        Text("Timezone", style: _style.fontStyleH7,),
                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text(schoolData.timezone, style: _style.fontStyleH7),))
                                                      ],),
                                                      SizedBox(height: 5),
                                                      Row(children: <Widget>[
                                                        Text("Keyboard Layout", style: _style.fontStyleH7,),
                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text(schoolData.keyboard_layout, style: _style.fontStyleH7),))
                                                      ],),
                                                      SizedBox(height: 5),
                                                      Row(children: <Widget>[
                                                        Text("Total Members", style: _style.fontStyleH7,),
                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text(schoolData.member_usernames.length.toString(), style: _style.fontStyleH7),))
                                                      ],),
                                                      SizedBox(height: 5),
                                                    ],
                                                  )))
                                        ],
                                      ))
                                    ]))
                                  ]))))
            ])));
  }
}
