import 'dart:async';

import 'package:flutter/material.dart';
import 'package:puavomobile/style/style.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:puavomobile/services/globals.dart' as globals;
//import 'package:puavomobile/style/error_dialog.dart';
//import 'package:puavomobile/pages/dashboard.dart';
//import 'package:puavomobile/services/connect_api.dart' as connect_api;

import 'dashboard.dart';
import 'domain.dart';
import 'loading.dart';

class ChangePass extends StatefulWidget {
  String domain;
  String title;
  String url;

  ChangePass({Key key, this.domain, this.title, this.url});

  @override
  _ChangePassState createState() => _ChangePassState();
}

class _ChangePassState extends State<ChangePass> {
//  WebViewController _webViewController;
  final Completer<WebViewController> _controller =
  Completer<WebViewController>();
  bool isLoad;

  final txtEmail = TextEditingController();
  final txtPassword = TextEditingController();
  Style _style;

  @override
  void initState() {
    globals.setContext(context);
    isLoad = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
//      appBar: AppBar(
//        title: Text("Change Password"),
//        backgroundColor: Colors.orange,
//        centerTitle: true,
//      ),
      body: Stack(
        children: <Widget>[
          Builder(builder: (BuildContext context) {
            return Container(
                child: WebView(
                  initialUrl: widget.url,
                  javascriptMode: JavascriptMode.unrestricted,
                  onWebViewCreated: (WebViewController webViewController) {
//                  _webViewController = webViewController;
                    _controller.complete(webViewController);
                    if (_controller.isCompleted) {
                      setState(() {
                        isLoad = false;
                      });
                    }
                  },
                  javascriptChannels: <JavascriptChannel>[
                    _toasterJavascriptChannel(context),
                  ].toSet(),
                  navigationDelegate: (NavigationRequest request) {
//                  if (request.url.startsWith('https://www.youtube.com/')) {
//                    return NavigationDecision.prevent;
//                  }
                    return NavigationDecision.navigate;
                  },
                  onPageStarted: (String url) {
                    print("start url "+url);
                  },
                  onPageFinished: (String url) {
                    print("end url "+url);
//                    if (url != "https://"+widget.domain+".opinsys.fi/users/password")
//                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>Dashboard(domain: widget.domain,)),);
//                  else {
//                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>DomainPage()),);
//                  }

                  },
                )
            );
          }),
          Visibility(
            visible: isLoad,
            child: LoadingDialogWidget(),
          ),
        ],
      ),
    );
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }
}
