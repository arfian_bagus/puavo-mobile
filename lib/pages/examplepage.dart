import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puavomobile/services/globals.dart' as globals;
import 'package:puavomobile/style/circle_loading.dart';
import 'package:puavomobile/style/style.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ExamplePage extends StatefulWidget {
  @override
  _ExamplePageState createState() => _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage> {
  Style _style;
  bool isLoad;
  ScrollController _controller = new ScrollController();
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  @override
  void initState() {
    globals.setContext(context);
    isLoad = false;
    super.initState();
  }

  void onRefresh() async {
    await Future.delayed(Duration(milliseconds: 500));

    refreshData();
    _refreshController.refreshCompleted();
    _refreshController.loadComplete();
  }

  Future<void> refreshData() async {}

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
        body: new Container(
            width: _style.widthScreen(),
            height: _style.heightScreen(),
            child: Stack(children: <Widget>[
              isLoad
                  ? WidgetCircleLoading()
                  : new Container(
                  child: new Container(
                      color: Colors.white,
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: new SmartRefresher(
                          enablePullDown: true,
                          enablePullUp: true,
                          header: MaterialClassicHeader(),
                          footer: CustomFooter(
                            builder:
                                (BuildContext context, LoadStatus mode) {
                              Widget body;
                              if (mode == LoadStatus.idle) {
                                body = Container();
                              } else if (mode == LoadStatus.loading) {
                                body = CupertinoActivityIndicator();
                              } else if (mode == LoadStatus.failed) {
                                body = Container();
                              } else if (mode == LoadStatus.canLoading) {
                                body = Container();
                              } else {
                                body = Container();
                              }
                              return Container(
                                height: 55.0,
                                child: Center(child: body),
                              );
                            },
                          ),
                          controller: _refreshController,
                          onRefresh: onRefresh,
                          child: new CustomScrollView(
                              controller: _controller,
                              physics: AlwaysScrollableScrollPhysics(),
                              shrinkWrap: true,
                              slivers: <Widget>[
                                SliverList(
                                    delegate: SliverChildListDelegate([
                                      new Container(
                                          child: new Column(
                                            children: <Widget>[

                                            ],
                                          ))
                                    ]))
                              ]))))
            ])));
  }
}
