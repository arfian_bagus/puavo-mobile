import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puavomobile/models/usermodel.dart';
import 'package:puavomobile/services/globals.dart' as globals;
import 'package:puavomobile/style/circle_loading.dart';
import 'package:puavomobile/style/style.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:puavomobile/services/helper.dart' as helper;

class DetailUserPage extends StatefulWidget {
  UserModel user;
  DetailUserPage({Key key, this.user});

  @override
  _DetailUserPageState createState() => _DetailUserPageState();
}

class _DetailUserPageState extends State<DetailUserPage> {
  Style _style;
  bool isLoad;
  ScrollController _controller = new ScrollController();
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  @override
  void initState() {
    globals.setContext(context);
    isLoad = false;
    super.initState();
  }

  void onRefresh() async {
    await Future.delayed(Duration(milliseconds: 500));

    refreshData();
    _refreshController.refreshCompleted();
    _refreshController.loadComplete();
  }

  Future<void> refreshData() async {}

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
        appBar: AppBar(
          title: Text("Detail User"),
          backgroundColor: Colors.orange,
          centerTitle: true,
        ),
        body: new Container(
            width: _style.widthScreen(),
            height: _style.heightScreen(),
            child: Stack(children: <Widget>[
              isLoad
                  ? WidgetCircleLoading()
                  : new Container(
                  child: new Container(
                      color: Colors.white,
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: new SmartRefresher(
                          enablePullDown: true,
                          enablePullUp: true,
                          header: MaterialClassicHeader(),
                          footer: CustomFooter(
                            builder:
                                (BuildContext context, LoadStatus mode) {
                              Widget body;
                              if (mode == LoadStatus.idle) {
                                body = Container();
                              } else if (mode == LoadStatus.loading) {
                                body = CupertinoActivityIndicator();
                              } else if (mode == LoadStatus.failed) {
                                body = Container();
                              } else if (mode == LoadStatus.canLoading) {
                                body = Container();
                              } else {
                                body = Container();
                              }
                              return Container(
                                height: 55.0,
                                child: Center(child: body),
                              );
                            },
                          ),
                          controller: _refreshController,
                          onRefresh: onRefresh,
                          child: new CustomScrollView(
                              controller: _controller,
                              physics: AlwaysScrollableScrollPhysics(),
                              shrinkWrap: true,
                              slivers: <Widget>[
                                SliverList(
                                    delegate: SliverChildListDelegate([
                                      new Container(
                                          child: new Column(
                                            children: <Widget>[
                                              new Container(
                                                margin: EdgeInsets.only(top: 10),
                                                child: Center(
                                                  child: ClipOval(
                                                    child: Container(
                                                      color: Colors.teal,
                                                      width: 100,
                                                      height: 100,
                                                      child: Container(
                                                          margin: EdgeInsets.only(top: 20, left: 20),
                                                          child:Text(helper.inisial(widget.user.reverse_name.length>0?widget.user.reverse_name:widget.user.first_name), style: TextStyle(color: Colors.white, fontSize: 50, fontWeight: FontWeight.bold),)),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              new Container(
                                                  margin:
                                                  EdgeInsets.only(bottom: 10),
                                                  child: new Center(
                                                      child: new Text(
                                                        widget.user.reverse_name.length>0?widget.user.reverse_name:widget.user.first_name,
                                                        style: TextStyle(
                                                            fontWeight: FontWeight.w500,
                                                            fontSize: 25),
                                                      ))),
                                              _style.widgetVerticalShadow(
                                                  title: "General Information",
                                                  space: 5,
                                                  child: new Container(
                                                      width: _style.widthScreen(),
                                                      padding: EdgeInsets.symmetric(
                                                          horizontal:
                                                          _style.paddingLeft()),
                                                      child: new Column(
                                                        children: <Widget>[
                                                          Row(
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons.play_arrow,
                                                                color:
                                                                Colors.orange,
                                                              ),
                                                              Text(
                                                                "Name :",
                                                                style: _style
                                                                    .fontStyleH7,
                                                              ),
                                                              Expanded(
                                                                  child: Align(
                                                                    alignment: Alignment
                                                                        .centerRight,
                                                                    child: Text(
                                                                        widget.user.reverse_name.length>0?widget.user.reverse_name:widget.user.first_name,
                                                                        style: _style
                                                                            .fontStyleH7),
                                                                  ))
                                                            ],
                                                          ),
                                                          SizedBox(height: 5),
                                                          Row(
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons.play_arrow,
                                                                color:
                                                                Colors.orange,
                                                              ),
                                                              Text(
                                                                "Username :",
                                                                style: _style
                                                                    .fontStyleH7,
                                                              ),
                                                              Expanded(
                                                                  child: Align(
                                                                    alignment: Alignment
                                                                        .centerRight,
                                                                    child: Text(
                                                                        widget.user
                                                                            .username,
                                                                        style: _style
                                                                            .fontStyleH7),
                                                                  ))
                                                            ],
                                                          ),
                                                          SizedBox(height: 5),
                                                          Row(
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons.play_arrow,
                                                                color:
                                                                Colors.orange,
                                                              ),
                                                              Text(
                                                                "Language :",
                                                                style: _style
                                                                    .fontStyleH7,
                                                              ),
                                                              Expanded(
                                                                  child: Align(
                                                                    alignment: Alignment
                                                                        .centerRight,
                                                                    child: Text(
                                                                        widget.user
                                                                            .preferred_language,
                                                                        style: _style
                                                                            .fontStyleH7),
                                                                  ))
                                                            ],
                                                          ),
                                                          SizedBox(height: 5),
                                                          Row(
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons.play_arrow,
                                                                color:
                                                                Colors.orange,
                                                              ),
                                                              Text(
                                                                "Type :",
                                                                style: _style
                                                                    .fontStyleH7,
                                                              ),
                                                              Expanded(
                                                                  child: Align(
                                                                    alignment: Alignment
                                                                        .centerRight,
                                                                    child: Text(
                                                                        widget.user
                                                                            .user_type,
                                                                        style: _style
                                                                            .fontStyleH7),
                                                                  ))
                                                            ],
                                                          ),
                                                          SizedBox(height: 5),
                                                          Row(
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons.play_arrow,
                                                                color:
                                                                Colors.orange,
                                                              ),
                                                              Text(
                                                                "Email :",
                                                                style: _style
                                                                    .fontStyleH7,
                                                              ),
                                                              Expanded(
                                                                  child: Align(
                                                                    alignment: Alignment
                                                                        .centerRight,
                                                                    child: Text(
                                                                        widget.user
                                                                            .email,
                                                                        style: _style
                                                                            .fontStyleH7),
                                                                  ))
                                                            ],
                                                          ),
                                                          SizedBox(height: 5),
                                                          Row(
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons.play_arrow,
                                                                color:
                                                                Colors.orange,
                                                              ),
                                                              Text(
                                                                "Timezone :",
                                                                style: _style
                                                                    .fontStyleH7,
                                                              ),
                                                              Expanded(
                                                                  child: Align(
                                                                    alignment: Alignment
                                                                        .centerRight,
                                                                    child: Text(
                                                                        widget.user
                                                                            .timezone,
                                                                        style: _style
                                                                            .fontStyleH7),
                                                                  ))
                                                            ],
                                                          ),
                                                          SizedBox(height: 5),
                                                          Row(
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons.play_arrow,
                                                                color:
                                                                Colors.orange,
                                                              ),
                                                              Text(
                                                                "Domain :",
                                                                style: _style
                                                                    .fontStyleH7,
                                                              ),
                                                              Expanded(
                                                                  child: Align(
                                                                    alignment: Alignment
                                                                        .centerRight,
                                                                    child: Text(
                                                                        widget.user
                                                                            .domain_username,
                                                                        style: _style
                                                                            .fontStyleH7),
                                                                  ))
                                                            ],
                                                          ),
                                                          SizedBox(height: 5),
                                                        ],
                                                      ))),
                                            ],
                                          ))
                                    ]))
                              ]))))
            ])));
  }
}
