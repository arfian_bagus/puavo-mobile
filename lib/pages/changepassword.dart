import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puavomobile/pages/drawerpage.dart';
import 'package:puavomobile/style/error_dialog.dart';
import 'package:puavomobile/style/style.dart';
import 'package:puavomobile/services/shared_pref.dart' as pref;
import 'package:puavomobile/services/auth.dart' as auth;
import 'package:puavomobile/services/globals.dart' as globals;

class ChangePasswordPage extends StatefulWidget {
  ChangePasswordPage({Key key});

  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> with SingleTickerProviderStateMixin {
  bool isLoad;
  String domain;
  String passwordOld;
  String username;

  final txtPassword = TextEditingController();
  final txtRetypePassword = TextEditingController();
  final txtEmail = TextEditingController();

  final txtPasswordSelf = TextEditingController();
  final txtOldPasswordSelf = TextEditingController();
  final txtRetypePasswordSelf = TextEditingController();
  
  TabController _controllerTab;
  int currentIndex = 0;

  bool isAutoValidation = false;
  bool isAutoValidation2 = false;
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  Style _style;

  @override
  void initState() {
    globals.setContext(context);
    isLoad = true;
    domain = "";
    passwordOld = "";
    username = "";
    currentIndex = 0;
    _controllerTab = new TabController(length: 2, vsync: this);
    getPassword();
    getUsername();
    super.initState();
  }

  getDomain() async {
    var temp = await pref.getDomain();
    if (temp != null) {
      setState(() {
        domain = temp;
      });
    }
  }

  getPassword() async {
    var temp = await pref.getPassword();
    if (temp != null) {
      setState(() {
        passwordOld = temp;
      });
    }
  }

  getUsername() async {
    var temp = await pref.getUsername();
    if (temp != null) {
      setState(() {
        username = temp;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
      appBar: AppBar(
       title: Text("Change Password"),
       backgroundColor: Colors.orange,
       centerTitle: true,
     ),
      body: new Container(
        padding: EdgeInsets.only(top: 30),
        alignment: Alignment.center,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/bgopensys.jpg"),
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.3), BlendMode.dstATop))),
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Center(
                child: new Image.asset(
                  "assets/opinsyslogo.png",
                  width: _style.widthScreen() / 1.8,
//                  height: _style.widthScreen() / 2.5,
                ),
              ),
            ),
            Container(child: Column(
              children: <Widget>[
                new Container(
                  height: 50,
                  child: new TabBar(
                    indicatorColor: Colors.teal,
                    labelColor: Colors.teal,
                    unselectedLabelColor: Colors.black,
                    controller: _controllerTab,
                    onTap: (index){
                      setState(() {
                        currentIndex = index;
                      });
                    },
                    tabs: [
                      new Tab(
                        text: 'Change my password',
                      ),
                      new Tab(
                        text: "Change user password",
                      ),
                    ],
                  ),
                ),

                new Container(
                  //  padding: EdgeInsets.only(bottom: 100),
                  height: (_style.heightScreen() - 180),
                  child: new TabBarView(
                    controller: _controllerTab,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 10, left: 30, right: 30),
                        child: Form(
                            key: _formKey2,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                TextFormField(
                                  controller: txtOldPasswordSelf,
                                  autovalidate: isAutoValidation2,
                                  cursorColor: Colors.teal,
                                  textAlign: TextAlign.center,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    filled: true,
                                    labelText: 'Old Password ',
                                    fillColor: Colors.white.withOpacity(0.2),
                                    labelStyle: TextStyle(color: Colors.teal),
                                    errorStyle: TextStyle(color: Color(0xFFFF7777)),
                                    border: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                    enabledBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                    errorBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.red)),
                                    focusedBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Enter Old Password';
                                    }
                                    return null;
                                  },
                                ),
                                Container(margin: EdgeInsets.only(top: 10),),
                                TextFormField(
                                  controller: txtPasswordSelf,
                                  autovalidate: isAutoValidation2,
                                  cursorColor: Colors.teal,
                                  textAlign: TextAlign.center,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    filled: true,
                                    labelText: 'New Password ',
                                    fillColor: Colors.white.withOpacity(0.2),
                                    labelStyle: TextStyle(color: Colors.teal),
                                    errorStyle: TextStyle(color: Color(0xFFFF7777)),
                                    border: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                    enabledBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                    errorBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.red)),
                                    focusedBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Enter New Password';
                                    }
                                    return null;
                                  },
                                ),
                                Container(margin: EdgeInsets.only(top: 10),),
                                TextFormField(
                                  controller: txtRetypePasswordSelf,
                                  autovalidate: isAutoValidation2,
                                  cursorColor: Colors.teal,
                                  textAlign: TextAlign.center,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    filled: true,
                                    labelText: 'Retype Password ',
                                    fillColor: Colors.white.withOpacity(0.2),
                                    labelStyle: TextStyle(color: Colors.teal),
                                    errorStyle: TextStyle(color: Color(0xFFFF7777)),
                                    border: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                    enabledBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                    errorBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.red)),
                                    focusedBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Enter Retype Password';
                                    }
                                    return null;
                                  },
                                ),
                                Container(
                                  margin: const EdgeInsets.only(top: 20.0),
                                  height: 50.0,
                                  width: 400.0,
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  child: FlatButton(
                                    color: Colors.orange,
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.lock_open,
                                            color: Colors.white,
                                          ),
                                          Container(
                                            child: Text(
                                              "Change Password",
                                              style: TextStyle(color: Colors.white),
                                            ),
                                            padding: EdgeInsets.symmetric(horizontal: 5),
                                          )
                                        ]),
                                    onPressed: () {
                                      isAutoValidation2 = true;
                                      if (_formKey2.currentState.validate()) {
                                        if (txtPasswordSelf.text == txtRetypePasswordSelf.text && passwordOld == txtOldPasswordSelf.text){
                                          auth.changePass(
                                            typeChange: "self",
                                            username: username,
                                            retype: txtRetypePasswordSelf.text,
                                            password: txtPasswordSelf.text)
                                            .then((user) {
                                            if (user){
                                              showDialog(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return AlertDialog(
                                                    title: Text("Opinsys"),
                                                    content: Text("Password successfully updated!"),
                                                    actions: <Widget>[
                                                      FlatButton(
                                                        // isDefaultAction: true,
                                                        onPressed: () {
                                                          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => DrawerPage()),);
                                                        },
                                                        child: Text("Close"),
                                                      )
                                                    ],
                                                  );
                                                },
                                              );
                                            } else {
                                              new ErrorDialog(context).setMessage("Change password failed!").show();
                                            }
                                          }).catchError((message) {
                                            if (message != "error connection")
                                              new ErrorDialog(context).setMessage("Change password failed!").show();
                                          });
                                        } else {
                                          new ErrorDialog(context).setMessage("Password does not match!").show();
                                        }
                                      }
                                    },
                                  ),
                                ),
                              ],
                            )),
                      ),

                      Container(
                        margin: EdgeInsets.only(top: 10, left: 30, right: 30),
                        child: Form(
                            key: _formKey,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                TextFormField(
                                  controller: txtEmail,
                                  autovalidate: isAutoValidation,
                                  cursorColor: Colors.teal,
                                  textAlign: TextAlign.center,
                                  decoration: InputDecoration(
                                    filled: true,
                                    labelText: 'Username ',
                                    fillColor: Colors.white.withOpacity(0.2),
                                    labelStyle: TextStyle(color: Colors.teal),
                                    errorStyle: TextStyle(color: Color(0xFFFF7777)),
                                    border: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                    enabledBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                    errorBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.red)),
                                    focusedBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Enter Username';
                                    }
                                    return null;
                                  },
                                ),
                                Container(margin: EdgeInsets.only(top: 10),),
                                TextFormField(
                                  controller: txtPassword,
                                  autovalidate: isAutoValidation,
                                  cursorColor: Colors.teal,
                                  textAlign: TextAlign.center,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    filled: true,
                                    labelText: 'New Password ',
                                    fillColor: Colors.white.withOpacity(0.2),
                                    labelStyle: TextStyle(color: Colors.teal),
                                    errorStyle: TextStyle(color: Color(0xFFFF7777)),
                                    border: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                    enabledBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                    errorBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.red)),
                                    focusedBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Enter New Password';
                                    }
                                    return null;
                                  },
                                ),
                                Container(margin: EdgeInsets.only(top: 10),),
                                TextFormField(
                                  controller: txtRetypePassword,
                                  autovalidate: isAutoValidation,
                                  cursorColor: Colors.teal,
                                  textAlign: TextAlign.center,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    filled: true,
                                    labelText: 'Retype Password ',
                                    fillColor: Colors.white.withOpacity(0.2),
                                    labelStyle: TextStyle(color: Colors.teal),
                                    errorStyle: TextStyle(color: Color(0xFFFF7777)),
                                    border: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                    enabledBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                    errorBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.red)),
                                    focusedBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Enter Retype Password';
                                    }
                                    return null;
                                  },
                                ),
                                Container(
                                  margin: const EdgeInsets.only(top: 20.0),
                                  height: 50.0,
                                  width: 400.0,
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  child: FlatButton(
                                    color: Colors.orange,
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.lock_open,
                                            color: Colors.white,
                                          ),
                                          Container(
                                            child: Text(
                                              "Change Password",
                                              style: TextStyle(color: Colors.white),
                                            ),
                                            padding: EdgeInsets.symmetric(horizontal: 5),
                                          )
                                        ]),
                                    onPressed: () {
                                      isAutoValidation = true;
                                      if (_formKey.currentState.validate()) {
                                        if (txtPassword.text == txtRetypePassword.text){
                                          auth.changePass(
                                            typeChange: "user",
                                            username: txtEmail.text,
                                            retype: txtRetypePassword.text,
                                            password: txtPassword.text)
                                            .then((user) {
          //                                  Navigator.pop(context);
                                            if (user){
                                              showDialog(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return AlertDialog(
                                                    title: Text("Opinsys"),
                                                    content: Text("Password successfully updated!"),
                                                    actions: <Widget>[
                                                      FlatButton(
                                                        // isDefaultAction: true,
                                                        onPressed: () {
                                                          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => DrawerPage()),);
                                                        },
                                                        child: Text("Close"),
                                                      )
                                                    ],
                                                  );
                                                },
                                              );
                                            } else {
                                              new ErrorDialog(context).setMessage("Change password failed!").show();
                                            }
                                          }).catchError((message) {
                                            if (message != "error connection")
                                              new ErrorDialog(context).setMessage("Change password failed!").show();
                                          });
                                        } else {
                                          new ErrorDialog(context).setMessage("Password does not match!").show();
                                        }
                                      }
                                    },
                                  ),
                                ),
                              ],
                            )),
                      )

                  ]))
              ],
            ),),
            
          ],
        ),
      ),
    );
  }
}
