import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puavomobile/models/devicemodel.dart';
import 'package:puavomobile/services/globals.dart' as globals;
import 'package:puavomobile/style/circle_loading.dart';
import 'package:puavomobile/style/error_dialog.dart';
import 'package:puavomobile/style/style.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:puavomobile/services/helper.dart' as helper;
import 'package:puavomobile/services/device_api.dart' as device_api;

import 'detaildevice.dart';

class DevicePage extends StatefulWidget {
  @override
  _DevicePageState createState() => _DevicePageState();
}

class _DevicePageState extends State<DevicePage> with SingleTickerProviderStateMixin {
  Style _style;
  bool isLoad;
  ScrollController _controller = new ScrollController();
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);
  List<DeviceModel> listDevice = new List();

  @override
  void initState() {
    globals.setContext(context);
    isLoad = false;
    refreshData();
    super.initState();
  }

  void onRefresh() async {
    await Future.delayed(Duration(milliseconds: 500));

    refreshData();
    _refreshController.refreshCompleted();
    _refreshController.loadComplete();
  }

  Future<void> refreshData() async {
    setState(() {
      isLoad = true;
    });
    device_api.getDevices().then((res) {
      if (res.length > 0) {
        setState(() {
          listDevice = res;
        });
      }
      setState(() {
        isLoad = false;
      });
    }).catchError((message) {
      if (message != "error connection")
        new ErrorDialog(context).setMessage(message).show();

      setState(() {
        isLoad = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
        body: new Container(
            width: _style.widthScreen(),
//            height: _style.heightScreen(),
            child: Stack(children: <Widget>[
              isLoad
                  ? WidgetCircleLoading()
                  : new Container(
                  child: new Container(
                      color: Colors.white,
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: new SmartRefresher(
                          enablePullDown: true,
                          enablePullUp: false,
                          header: MaterialClassicHeader(),
                          controller: _refreshController,
                          onRefresh: onRefresh,
                          child: new CustomScrollView(
                              controller: _controller,
                              physics: AlwaysScrollableScrollPhysics(),
                              shrinkWrap: true,
                              slivers: <Widget>[
                                SliverList(
                                    delegate: SliverChildListDelegate([
                                      new Container(
                                        child: new ListView.builder(
                                            scrollDirection: Axis.vertical,
                                            physics: ClampingScrollPhysics(),
                                            shrinkWrap: true,
                                            itemCount: listDevice.length,
                                            itemBuilder: (context,index){
                                              return GestureDetector(onTap: (){
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) => DetailDevicePage(device: listDevice[index],),
                                                    ));
                                              }, child: Container(
                                                  margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                                                  padding: EdgeInsets.only(top: 10),
                                                  decoration: BoxDecoration(
                                                    border: Border(
                                                        top: BorderSide(
                                                            color: index == 0 ? Colors.white : Colors.grey[300], width: 1)),
                                                  ),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
//                                                                      Row(children: <Widget>[
//                                                                        Text("Test", style: TextStyle(fontWeight : FontWeight.bold, fontSize: 20),),
//                                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text("Text"),))
//                                                                      ]),
                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.end,
                                                        children: <Widget>[
                                                          ClipOval(
                                                            child: Container(
                                                              color: index%2==0 ? Colors.orange : Colors.teal,
                                                              width: 50,
                                                              height: 50,
                                                              child: Container(
                                                                  child: new Icon(Icons.computer, color: Colors.white, size: 30,),
                                                              ),
                                                            ),
                                                          ),
                                                          Expanded(
                                                              child: Container(
                                                                margin: EdgeInsets.only(left: 10),
                                                                child: Column(children: <Widget>[
                                                                  Row(children: <Widget>[
                                                                    Text(listDevice[index].hostname, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
//                                                                                    Expanded( child: Align(alignment: Alignment.centerRight, child: Text(listUser[index].reverse_name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),)))
                                                                  ],),
                                                                  Row(children: <Widget>[
                                                                    Text(listDevice[index].model),
//                                                                    Text(listDevice[index].hwobj.battery["percentage"].toString()),
                                                                    Expanded( child: Align(alignment: Alignment.centerRight, child: Text(listDevice[index].type),))
                                                                  ],),
                                                                  Row(children: <Widget>[
                                                                    Text("Battery " + listDevice[index].hwobj.battery["percentage"].toString()),
                                                                    Expanded( child: Align(alignment: Alignment.centerRight, child: Text(helper.timeStampToDate(listDevice[index].hwobj.timestamp),)))
                                                                  ],),
                                                                ],),
                                                              )
                                                          )
                                                        ],),
                                                    ],)
                                              ));}
                                        ),
                                      ),
                                    ]))
                              ]))))
            ])));
  }
}
