import 'package:puavomobile/style/style.dart';
import 'package:flutter/material.dart';

class LoadingDialogWidget extends StatelessWidget {
  Color colorBackground = Colors.black.withOpacity(0.5);
  String message;
  LoadingDialogWidget(
      {this.colorBackground, this.message = "Tunggu Sebentar.."});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Container(
        alignment: AlignmentDirectional.center,
        child: new Container(
          width: double.infinity,
          height: double.infinity,
          alignment: AlignmentDirectional.center,
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Center(
                child: new SizedBox(
                  height: 50.0,
                  width: 50.0,
                  child: new CircularProgressIndicator(
                    value: null,
                    strokeWidth: 7.0,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Material(
                  child: Text(
                    this.message,
                    overflow: TextOverflow.ellipsis,
                    style: new TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
