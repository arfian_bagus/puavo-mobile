import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puavomobile/models/devicemodel.dart';
import 'package:puavomobile/services/globals.dart' as globals;
import 'package:puavomobile/style/circle_loading.dart';
import 'package:puavomobile/style/error_dialog.dart';
import 'package:puavomobile/style/style.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:puavomobile/services/helper.dart' as helper;
import 'package:puavomobile/services/device_api.dart' as device_api;

import 'detaildevice.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> with SingleTickerProviderStateMixin {
  Style _style;
  bool isLoad;
  ScrollController _controller = new ScrollController();
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);
  List<DeviceModel> listSession = new List();
  List<DeviceModel> listBattery= new List();
  List<DeviceModel> listWarning= new List();
  TabController _controllerTab;
  int currentIndex = 0;

  @override
  void initState() {
    globals.setContext(context);
    isLoad = false;
    currentIndex = 0;
    _controllerTab = new TabController(length: 3, vsync: this);
    refreshData();
    super.initState();
  }

  void onRefresh() async {
    await Future.delayed(Duration(milliseconds: 500));

    refreshData();
    _refreshController.refreshCompleted();
    _refreshController.loadComplete();
  }

  sortData(String type, List<DeviceModel> res) {
    if(type == "session") {
      setState(() {
        listSession.clear();
        res.sort((a, b) => a.hwobj.timestamp.compareTo(b.hwobj.timestamp));
        listSession.addAll(res);
        if (res.length > 0) {
          listWarning.add(res[0]);
          listWarning[(listWarning.length-1)].note = "Last turned on since " + helper.timeStampToDate(listWarning[(listWarning.length-1)].hwobj.timestamp);
        }

        isLoad = false;
      });
    } else if (type == "percentage") {
      setState(() {
        listBattery.clear();
        for (var o in res) {
          String btr = o.hwobj.battery["percentage"].toString();
          o.percentageInt = double.parse(btr.substring(0, btr.length - 1));
        }
        res.sort((a, b) => a.percentageInt.compareTo(b.percentageInt));
        listBattery.addAll(res);
        if (res.length > 0) {
          listWarning.add(res[0]);
          listWarning[(listWarning.length-1)].note = "Critical battery "+listWarning[(listWarning.length-1)].hwobj.battery["percentage"];
        }

        isLoad = false;
      });
    }
  }

  Future<void> refreshData() async {
    setState(() {
      isLoad = true;
    });
    device_api.getDevices().then((res) {
      if (res.length > 0) {
        sortData("session", res);
        sortData("percentage", res);
      }
    }).catchError((message) {
      if (message != "error connection")
        new ErrorDialog(context).setMessage(message).show();

      setState(() {
        isLoad = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
        appBar: AppBar(
        title: Text("Notification"),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
        body: new Container(
            width: _style.widthScreen(),
//            height: _style.heightScreen(),
            child: Stack(children: <Widget>[
              isLoad
                  ? WidgetCircleLoading()
                  : new Container(
                  child: new Container(
                      color: Colors.white,
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: new SmartRefresher(
                          enablePullDown: true,
                          enablePullUp: false,
                          header: MaterialClassicHeader(),
                          controller: _refreshController,
                          onRefresh: onRefresh,
                          child: new CustomScrollView(
                              controller: _controller,
                              physics: AlwaysScrollableScrollPhysics(),
                              shrinkWrap: true,
                              slivers: <Widget>[
                                SliverList(
                                    delegate: SliverChildListDelegate([
                                      new Container(
                                          child: new Column(
                                            children: <Widget>[
                                              new Container(
                                                height: 80,
                                                child: new TabBar(
                                                  indicatorColor: Colors.teal,
                                                  labelColor: Colors.teal,
                                                  unselectedLabelColor: Colors.black,
                                                  controller: _controllerTab,
                                                  onTap: (index){
                                                    setState(() {
                                                      currentIndex = index;
                                                    });
                                                  },
                                                  tabs: [
                                                    new Tab(
                                                      icon: Icon(Icons.warning, color: currentIndex == 0 ? Colors.teal : Colors.black, size: 40),
                                                      text: 'Warning',
                                                    ),
                                                    new Tab(
                                                      icon: Icon(Icons.timer, color: currentIndex == 1 ? Colors.teal : Colors.black, size: 40),
                                                      text: 'Last Open',
                                                    ),
                                                    new Tab(
                                                      icon: Icon(Icons.battery_alert, color: currentIndex == 2 ? Colors.teal : Colors.black, size: 40),
                                                      text: 'Critical Battery',
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              new Container(
//                                                padding: EdgeInsets.only(bottom: 100),
                                                height: (_style.heightScreen() - 180),
                                                child: new TabBarView(
                                                  controller: _controllerTab,
                                                  children: <Widget>[
                                                    new Container(
                                                      child: new ListView.builder(
                                                          scrollDirection: Axis.vertical,
                                                          physics: ClampingScrollPhysics(),
                                                          shrinkWrap: true,
                                                          itemCount: listWarning.length,
                                                          itemBuilder: (context,index){
                                                            return GestureDetector(onTap: (){
                                                              Navigator.push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                    builder: (context) => DetailDevicePage(device: listWarning[index],),
                                                                  ));
                                                            }, child: Container(
                                                                margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                                                                padding: EdgeInsets.only(top: 10),
                                                                decoration: BoxDecoration(
                                                                  border: Border(
                                                                      top: BorderSide(
                                                                          color: index == 0 ? Colors.white : Colors.grey[300], width: 1)),
                                                                ),
                                                                child: Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: <Widget>[
//                                                                      Row(children: <Widget>[
//                                                                        Text("Test", style: TextStyle(fontWeight : FontWeight.bold, fontSize: 20),),
//                                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text("Text"),))
//                                                                      ]),
                                                                    Row(
                                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                                      children: <Widget>[
                                                                        ClipOval(
                                                                          child: Container(
                                                                            color: index%2==0 ? Colors.orange : Colors.teal,
                                                                            width: 50,
                                                                            height: 50,
                                                                            child: Container(
                                                                              child: new Icon(Icons.computer, color: Colors.white, size: 30,),),
                                                                          ),
                                                                        ),
                                                                        Expanded(
                                                                            child: Container(
                                                                              margin: EdgeInsets.only(left: 10),
                                                                              child: Column(children: <Widget>[
                                                                                Row(children: <Widget>[
                                                                                  Text(listWarning[index].hostname, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
//                                                                                    Expanded( child: Align(alignment: Alignment.centerRight, child: Text(listUser[index].reverse_name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),)))
                                                                                ],),
                                                                                Row(children: <Widget>[
                                                                                  Text(listWarning[index].note),
//                                                                                  Expanded( child: Align(alignment: Alignment.centerRight, child: Text(listUser[index].user_type),))
                                                                                ],),
                                                                              ],),
                                                                            )
                                                                        )
                                                                      ],),
                                                                  ],)
                                                            ),);}
                                                      ),
                                                    ),

                                                    new Container(
                                                      child: new ListView.builder(
                                                          scrollDirection: Axis.vertical,
                                                          physics: ClampingScrollPhysics(),
                                                          shrinkWrap: true,
                                                          itemCount: listSession.length,
                                                          itemBuilder: (context,index){
                                                            return GestureDetector(onTap: (){
                                                              Navigator.push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                    builder: (context) => DetailDevicePage(device: listSession[index],),
                                                                  ));
                                                            }, child: Container(
                                                                margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                                                                padding: EdgeInsets.only(top: 10),
                                                                decoration: BoxDecoration(
                                                                  border: Border(
                                                                      top: BorderSide(
                                                                          color: index == 0 ? Colors.white : Colors.grey[300], width: 1)),
                                                                ),
                                                                child: Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: <Widget>[
//                                                                      Row(children: <Widget>[
//                                                                        Text("Test", style: TextStyle(fontWeight : FontWeight.bold, fontSize: 20),),
//                                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text("Text"),))
//                                                                      ]),
                                                                    Row(
                                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                                      children: <Widget>[
                                                                        ClipOval(
                                                                          child: Container(
                                                                            color: index%2==0 ? Colors.orange : Colors.teal,
                                                                            width: 50,
                                                                            height: 50,
                                                                            child: Container(
                                                                              child: new Icon(Icons.computer, color: Colors.white, size: 30,),),
                                                                          ),
                                                                        ),
                                                                        Expanded(
                                                                            child: Container(
                                                                              margin: EdgeInsets.only(left: 10),
                                                                              child: Column(children: <Widget>[
                                                                                Row(children: <Widget>[
                                                                                  Text(listSession[index].hostname, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
//                                                                                    Expanded( child: Align(alignment: Alignment.centerRight, child: Text(listUser[index].reverse_name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),)))
                                                                                ],),
                                                                                Row(children: <Widget>[
                                                                                  Text("Last Open "+helper.timeStampToDate(listSession[index].hwobj.timestamp)),
//                                                                                  Expanded( child: Align(alignment: Alignment.centerRight, child: Text(listUser[index].user_type),))
                                                                                ],),
                                                                              ],),
                                                                            )
                                                                        )
                                                                      ],),
                                                                  ],)
                                                            ),);}
                                                      ),
                                                    ),
                                                    new Container(
                                                      child: new ListView.builder(
                                                          scrollDirection: Axis.vertical,
                                                          physics: ClampingScrollPhysics(),
                                                          shrinkWrap: true,
                                                          itemCount: listBattery.length,
                                                          itemBuilder: (context,index){
                                                            return GestureDetector(onTap: (){
                                                              Navigator.push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                    builder: (context) => DetailDevicePage(device: listBattery[index],),
                                                                  ));
                                                            }, child: Container(
                                                                margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                                                                padding: EdgeInsets.only(top: 10),
                                                                decoration: BoxDecoration(
                                                                  border: Border(
                                                                      top: BorderSide(
                                                                          color: index == 0 ? Colors.white : Colors.grey[300], width: 1)),
                                                                ),
                                                                child: Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: <Widget>[
//                                                                      Row(children: <Widget>[
//                                                                        Text("Test", style: TextStyle(fontWeight : FontWeight.bold, fontSize: 20),),
//                                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text("Text"),))
//                                                                      ]),
                                                                    Row(
                                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                                      children: <Widget>[
                                                                        ClipOval(
                                                                          child: Container(
                                                                            color: index%2==0 ? Colors.orange : Colors.teal,
                                                                            width: 50,
                                                                            height: 50,
                                                                            child: Container(
                                                                              child: new Icon(Icons.computer, color: Colors.white, size: 30,),),
                                                                          ),
                                                                        ),
                                                                        Expanded(
                                                                            child: Container(
                                                                              margin: EdgeInsets.only(left: 10),
                                                                              child: Column(children: <Widget>[
                                                                                Row(children: <Widget>[
                                                                                  Text(listBattery[index].hostname, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
//                                                                                    Expanded( child: Align(alignment: Alignment.centerRight, child: Text(listUser[index].reverse_name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),)))
                                                                                ],),
                                                                                Row(children: <Widget>[
                                                                                  Text("Battery "+listBattery[index].hwobj.battery["percentage"]),
//                                                                                  Expanded( child: Align(alignment: Alignment.centerRight, child: Text(listUser[index].user_type),))
                                                                                ],),
                                                                              ],),
                                                                            )
                                                                        )
                                                                      ],),
                                                                  ],)
                                                            ));}
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                            ],
                                          ))
                                    ]))
                              ]))))
            ])));
  }
}
