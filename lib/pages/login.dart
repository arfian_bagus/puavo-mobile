import 'dart:async';

import 'package:flutter/material.dart';
import 'package:puavomobile/style/error_dialog.dart';
import 'package:puavomobile/style/style.dart';
import 'package:puavomobile/services/shared_pref.dart' as pref;
import 'package:puavomobile/services/auth.dart' as auth;
import 'package:puavomobile/services/globals.dart' as globals;
//import 'package:opinsys/style/error_dialog.dart';
//import 'package:opinsys/pages/dashboard.dart';
//import 'package:opinsys/services/connect_api.dart' as connect_api;

//import 'dashboard.dart';
import 'drawerpage.dart';
import 'homepage.dart';
//import 'loading.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isLoad;
  String domain;

  final txtEmail = TextEditingController();
  final txtPassword = TextEditingController();
  bool isAutoValidation = false;
  final _formKey = GlobalKey<FormState>();
  Style _style;

  @override
  void initState() {
    globals.setContext(context);
    isLoad = true;
    domain = "";
    super.initState();
  }

  getDomain() async {
    var temp = await pref.getDomain();
    if (temp != null) {
      setState(() {
        domain = temp;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
//      appBar: AppBar(
//        title: Text("Login"),
//        backgroundColor: Colors.orange,
//        centerTitle: true,
//      ),
      body: new Container(
        alignment: Alignment.center,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/bgopensys.jpg"),
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.3), BlendMode.dstATop))),
        child: ListView(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Center(
                child: new Image.asset(
                  "assets/opinsyslogo.png",
                  width: _style.widthScreen() / 1.8,
//                  height: _style.widthScreen() / 2.5,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10, left: 30, right: 30),
              child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextFormField(
                        controller: txtEmail,
                        autovalidate: isAutoValidation,
                        cursorColor: Colors.teal,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          filled: true,
                          labelText: 'Username ',
                          fillColor: Colors.white.withOpacity(0.2),
                          labelStyle: TextStyle(color: Colors.teal),
                          errorStyle: TextStyle(color: Color(0xFFFF7777)),
                          border: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.teal)),
                          enabledBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.teal)),
                          errorBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.red)),
                          focusedBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.teal)),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Username';
                          }
                          return null;
                        },
                      ),
                      Container(margin: EdgeInsets.only(top: 20),),
                      TextFormField(
                        controller: txtPassword,
                        autovalidate: isAutoValidation,
                        cursorColor: Colors.teal,
                        obscureText: true,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          filled: true,
                          labelText: 'Password ',
                          fillColor: Colors.white.withOpacity(0.2),
                          labelStyle: TextStyle(color: Colors.teal),
                          errorStyle: TextStyle(color: Color(0xFFFF7777)),
                          border: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.teal)),
                          enabledBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.teal)),
                          errorBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.red)),
                          focusedBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.teal)),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Password';
                          }
                          return null;
                        },
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 20.0),
                        height: 50.0,
                        width: 400.0,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: FlatButton(
                          color: Colors.orange,
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.lock_open,
                                  color: Colors.white,
                                ),
                                Container(
                                  child: Text(
                                    "Log In",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  padding: EdgeInsets.symmetric(horizontal: 5),
                                )
                              ]),
                          onPressed: () {
                            isAutoValidation = true;
                            if (_formKey.currentState.validate()) {
                              auth.loginDo(
                                      username: txtEmail.text,
                                      password: txtPassword.text)
                                  .then((user) {
                                print(user.toString());
                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => DrawerPage()),);
//                                new ErrorDialog(context).setMessage("Login success").sukses();
                              }).catchError((message) {
                                pref.deleteUsername();
                                pref.deletePassword();
                                if (message != "error connection")
                                  new ErrorDialog(context).setMessage("Login failed!").show();
                              });
                            }
                          },
                        ),
                      ),
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }
}
