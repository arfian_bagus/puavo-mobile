import 'dart:io';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:puavomobile/style/error_dialog.dart';
import 'package:puavomobile/style/style.dart';
import 'package:puavomobile/pages/login.dart';
import 'package:puavomobile/services/globals.dart' as globals;
import 'package:puavomobile/services/shared_pref.dart' as pref;
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'changepass.dart';
import 'changepassword.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Style _style;
  bool isAutoValidation = false;
  String domain;
  String timezone;
  String typeCrop = "photo";
  File _imgfile;
  File _imgcrop;
  String photoUser = "";

  @override
  void initState() {
    globals.setContext(context);
    domain = "";
    timezone = "";
    getDomain();
    getPhoto();
    geTimeZone();
    super.initState();
  }

  getDomain() async {
    var temp = await pref.getDomain();
    if (temp != null) {
      setState(() {
        domain = temp;
      });
    }
  }

  geTimeZone() async {
    var temp = await pref.getTimeZone();
    if (temp != null) {
      setState(() {
        timezone = temp;
      });
    }
  }

  getPhoto() async {
    var temp = await pref.getPhotoProfile();
    if (temp != null) {
      setState(() {
        _imgcrop = File(temp);
      });
    }
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      setState(() {
        _imgfile = image;
        cropImage();
      });
    }

  }

  Future getCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    if (image != null) {
      setState(() {
        _imgfile = image;
        cropImage();
      });
    }
  }

  selectImage() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Select image source"),
//          content: Text(this.message),
          actions: <Widget>[
            FlatButton(
              child: Text('Camera'),
              onPressed: () {
                Navigator.pop(context);
                getCamera();
              },
            ),
            FlatButton(
              child: Text('Gallery'),
              onPressed: () {
                Navigator.pop(context);
                getImage();
              },
            ),
          ],
        );
      },
    );
  }

  Future cropImage() async {
    File croppedFile = await ImageCropper.cropImage(
      sourcePath: _imgfile.path,
      androidUiSettings: AndroidUiSettings(
          toolbarTitle: 'Crop Image',
          toolbarColor: Colors.deepOrange,
          toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.original,
          lockAspectRatio: false),
      iosUiSettings: IOSUiSettings(
        minimumAspectRatio: 1.0,
      ),
      aspectRatioPresets: [CropAspectRatioPreset.square],
      maxWidth: 500,
      maxHeight: 500,
    );
    if (croppedFile != null) {
      _savePhoto(croppedFile);
      setState(() {
        photoUser = "";
        _imgcrop = croppedFile;
      });
    }
  }

  Future _savePhoto(File image) async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String appDocPath = appDocDir.path;
    File newImage = await image.copy('$appDocPath/photoprofile.png');
    pref.deletePhotoProfile();
    pref.addPhotoProfile('$appDocPath/photoprofile.png');
  }

  Future _scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        new ErrorDialog(context).setMessage("Your QR Code : $qrResult").show();
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          // result = "Camera permission was denied";
          new ErrorDialog(context).setMessage("Camera permission was denied").show();
        });
      } else {
        setState(() {
          // result = "Unknown Error $ex";
          new ErrorDialog(context).setMessage("Unknown Error $ex").show();
        });
      }
    } on FormatException {
      setState(() {
        // result = "You pressed the back button before scanning anything";
      });
    } catch (ex) {
      setState(() {
        // result = "Unknown Error $ex";
        new ErrorDialog(context).setMessage("Unknown Error $ex").show();
      });
    }
  }

  Future check(String type) async {
    if (type == "login") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      );
    } else if (type == "change") {
//      Navigator.push(
//        context,
//        MaterialPageRoute(
//            builder: (context) =>
//                ChangePass(
//                  domain: domain,
//                  url: "https://" + domain + ".opinsys.fi/users/password",
//                  title: "Change Password",
//                )),
//      );
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChangePasswordPage(),
          ));
    } else if (type == "support"){
      if (timezone == "Asia/Jakarta"){
        var whatsappUrl ="whatsapp://send?phone=+6281938495217";
        await canLaunch(whatsappUrl)? launch(whatsappUrl):new ErrorDialog(context).setMessage("There is no whatsapp installed!").show();
      } else {
        launch("tel://0144591624");
      }
    }

  }

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
      body: new Container(
        alignment: Alignment.center,
        decoration: new BoxDecoration(
          image: new DecorationImage(
              image: new AssetImage("assets/bgopensys.jpg"),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.3), BlendMode.dstATop)),
        ),
        child: ListView(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Center(
                child: new GestureDetector(
                    onTap: () {
                      setState(() {
                        typeCrop = "photo";
                      });
                      selectImage();
                    },
                    child: Container(
                        height: 120.0,
                        width: 120.0,
                        decoration: new BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: _imgcrop == null
                                    ? new AssetImage("assets/person.png")
                                    : new FileImage(_imgcrop),
                                fit: BoxFit.contain)))
//                      : ClipOval(
//                    child: Container(
//                      height: 200.0,
//                      width: 200.0,
//                      child: Image.network(url_connect.base_url + "/datasales/" + photoUser, fit: BoxFit.cover,),
//                    ))
                    ),
              ),
            ),
            Center(
                child: Text(
              "Welcome " + domain,
              style: TextStyle(fontSize: 20),
            )),
            Container(
              margin: EdgeInsets.only(top: 10, left: 30, right: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // Container(
                  //   margin: const EdgeInsets.only(top: 20.0),
                  //   height: 50.0,
                  //   width: 400.0,
                  //   decoration: BoxDecoration(
                  //     color: Colors.red,
                  //     shape: BoxShape.rectangle,
                  //     borderRadius: BorderRadius.circular(50),
                  //   ),
                  //   child: FlatButton(
                  //     color: Colors.orange,
                  //     child: Row(
                  //         // R
                  //         mainAxisAlignment: MainAxisAlignment
                  //             .center, // eplace with a Row for horizontal icon + text
                  //         children: <Widget>[
                  //           Icon(
                  //             Icons.camera_enhance,
                  //             color: Colors.white,
                  //           ),
                  //           Container(
                  //             child: Text(
                  //               "Scan QR",
                  //               style: TextStyle(color: Colors.white),
                  //             ),
                  //             padding: EdgeInsets.symmetric(horizontal: 5),
                  //           )
                  //         ]),
                  //     onPressed: () {
                  //       _scanQR();
                  //     },
                  //   ),
                  // ),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    height: 50.0,
                    width: 400.0,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: FlatButton(
                      color: Colors.orange,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.refresh,
                              color: Colors.white,
                            ),
                            Container(
                              child: Text(
                                "Change Password",
                                style: TextStyle(color: Colors.white),
                              ),
                              padding: EdgeInsets.symmetric(horizontal: 5),
                            )
                          ]),
                      onPressed: () {
                        check("change");
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    height: 50.0,
                    width: 400.0,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: FlatButton(
                      color: Colors.orange,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.chat,
                              color: Colors.white,
                            ),
                            Container(
                              child: Text(
                                "Support",
                                style: TextStyle(color: Colors.white),
                              ),
                              padding: EdgeInsets.symmetric(horizontal: 5),
                            )
                          ]),
                      onPressed: () {
                        check("support");
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
