import 'package:flutter/material.dart';
import 'package:puavomobile/models/schoolmodel.dart';
import 'package:puavomobile/pages/splashscreen.dart';
import 'package:puavomobile/services/shared_pref.dart' as pref;
import 'package:puavomobile/services/globals.dart' as globals;
import 'package:puavomobile/style/error_dialog.dart';
import 'package:puavomobile/services/school_api.dart' as school_api;
import 'package:url_launcher/url_launcher.dart';

import 'changepass.dart';
import 'changepassword.dart';
import 'devicepage.dart';
import 'devicestatistic.dart';
import 'homepage.dart';
import 'notificationpage.dart';
import 'school.dart';
import 'user.dart';

class DrawerItem {
  String title;
  IconData icon;
  DrawerItem(this.title, this.icon);
}

class DrawerPage extends StatefulWidget {
  final drawerItems = [
    new DrawerItem("Home", Icons.home),
    new DrawerItem("School", Icons.school),
    new DrawerItem("Users", Icons.person),
//    new DrawerItem("User Groups", Icons.people),
//    new DrawerItem("Username Lists", Icons.verified_user),
    new DrawerItem("Devices", Icons.devices),
//    new DrawerItem("Device Statistics", Icons.devices_other),
//    new DrawerItem("Printing Permissions", Icons.print),
//    new DrawerItem("External Services", Icons.settings),
    new DrawerItem("Change Password", Icons.refresh),
    new DrawerItem("Support", Icons.chat),
    new DrawerItem("Logout", Icons.lock)
  ];

  @override
  State<StatefulWidget> createState() {
    return new DrawerPageState();
  }
}

class DrawerPageState extends State<DrawerPage> {
  int _selectedDrawerIndex = 0;
  String namaUser = "";
  List<SchoolModel> listSchool = new List();
  String domain;
  String timezone;

  @override
  void initState() {
    globals.setContext(context);
    domain = "";
    timezone = "";
    getDomain();
    getNama();
    geTimeZone();
    refreshData();
    super.initState();
  }

  geTimeZone() async {
    var temp = await pref.getTimeZone();
    if (temp != null) {
      setState(() {
        timezone = temp;
      });
    }
  }

  Future<void> refreshData() async {
    var school = await pref.getIdSchool();
    var username = await pref.getUsername();
    bool schoolVal = true;
    school_api.getSchool().then((res) {
      if (res.length > 0) {
        for(var s in res){
          if(school == s.id){
            setState(() {
              globals.schoolData = s;
              pref.addIdSchool(s.id);
              schoolVal = false;
            });
          }

          for(var a in s.member_usernames){
            if(a == username)
              listSchool.add(s);
          }
        }
        if(schoolVal) {
          setState(() {
            globals.schoolData = res[0];
            pref.addIdSchool(res[0].id);
          });
        }
        setState(() {});
      }
    }).catchError((message) {
      if (message != "error connection")
        new ErrorDialog(context).setMessage(message).show();
    });
  }

  getDomain() async {
    var temp = await pref.getDomain();
    if (temp != null) {
      setState(() {
        domain = temp;
      });
    }
  }

  _getDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
        return new HomePage();
      case 1:
        return new SchoolPage();
      case 2:
        return new UserPage();
      case 3:
        return new DevicePage();
//      case 4:
//        return new DeviceStatisticPage();
//      case 5:
//        return new Center(child: Text("No print detected"));
      case 4:
//        return new ChangePass(
//          domain: domain,
//          url: "https://" + domain + ".opinsys.fi/users/password",
//          title: "Change Password",
//        );
        return new Text("Error");
      case 5:
        return new Text("Error");
      case 6:
        return new Text("Error");
      default:
        return new Text("Error");
    }
  }

  getNama() async {
    var temp = await pref.getNama();
    if (temp != null) {
      setState(() {
        namaUser = temp;
      });
    }
  }

  _onSelectItem(int index) {
    if (index < 4) {
      setState(() => _selectedDrawerIndex = index);
      Navigator.of(context).pop();
    } else if (index == 5) {
      launchWa();
    } else if (index == 4) {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChangePasswordPage(),
          ));
    } else {
      pref.deletePassword();
      pref.deleteUsername();
      pref.deleteDomain();
      pref.deleteIdSchool();
      pref.deleteNama();
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => SplashScreen()),
      );
    }
  }

  Future launchWa() async {
//    var whatsappUrl ="whatsapp://send?phone=+6281938495217";
//    await canLaunch(whatsappUrl)? launch(whatsappUrl):new ErrorDialog(context).setMessage("There is no whatsapp installed!").show();
    if (timezone == "Asia/Jakarta"){
      var whatsappUrl ="whatsapp://send?phone=+6281938495217";
      await canLaunch(whatsappUrl)? launch(whatsappUrl):new ErrorDialog(context).setMessage("There is no whatsapp installed!").show();
    } else {
      launch("tel://0144591624");
    }
  }

  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];
    for (var i = 0; i < widget.drawerItems.length; i++) {
      var d = widget.drawerItems[i];
      drawerOptions.add(new ListTile(
        leading: new Icon(d.icon, color: Colors.teal),
        title: new Text(d.title, style: TextStyle(color: Colors.teal)),
        selected: i == _selectedDrawerIndex,
        onTap: () => _onSelectItem(i),
      ));
    }

    return new WillPopScope(
        onWillPop: () {
          new ErrorDialog(context).showExitDialog();
        },
        child: new Scaffold(
          appBar: new AppBar(
            backgroundColor: Colors.orange,
            title: new Text(widget.drawerItems[_selectedDrawerIndex].title),
            actions: <Widget>[
              IconButton(icon: Icon(Icons.notification_important), onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => NotificationPage(),
                    ));
              }),
            ],
          ),
          drawer: new Drawer(
            child: new Column(
              children: <Widget>[
                new UserAccountsDrawerHeader(
//                  accountName: new Text(namaUser, style: TextStyle(color: Colors.teal, fontSize: 22),),
                accountName: new Text(namaUser, style: TextStyle(color: Colors.teal, fontSize: 22),),
                  accountEmail: new DropdownButton<SchoolModel>(
                    value: globals.schoolData,
                    onChanged: (SchoolModel newValue) {
                      setState(() {
                        globals.schoolData = newValue;
                        pref.addIdSchool(newValue.id);
                      });
                    },
                    items: listSchool.map((SchoolModel school) {
                      return new DropdownMenuItem<SchoolModel>(
                        value: school,
                        child: new Text(
                          school.name,
                          style: new TextStyle(color: Colors.black),
                        ),
                      );
                    }).toList(),
                  ),
                  decoration: BoxDecoration(
                      image: new DecorationImage(
                        alignment: Alignment(0, -0.3),
                    image: new AssetImage("assets/opinsyslogo.png"),
                  )),
                ),
                new Column(children: drawerOptions)
              ],
            ),
          ),
          body: _getDrawerItemWidget(_selectedDrawerIndex),
        ));
  }
}
