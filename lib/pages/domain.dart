import 'package:flutter/material.dart';
import 'package:puavomobile/style/style.dart';
import 'package:puavomobile/style/error_dialog.dart';
import 'package:puavomobile/services/globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'package:puavomobile/services/shared_pref.dart' as pref;
//import 'login.dart';
import 'login.dart';
import 'menu.dart';

class DomainPage extends StatefulWidget {
  @override
  _DomainPageState createState() => _DomainPageState();
}

class _DomainPageState extends State<DomainPage> {
  final txtDomain = TextEditingController();
  Style _style;
  bool isAutoValidation = false;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    globals.setContext(context);
    super.initState();
  }

  Future check(String type) async {
    if (txtDomain.text.isNotEmpty) {
      setState(() {
        isAutoValidation = true;
      });
      http
          .head("https://" + txtDomain.text + ".opinsys.fi/users/login")
          .catchError((err) {
        new ErrorDialog(context).setMessage("Domain not found").show();
      }).then((res) {
        if (res.statusCode == 200) {
          globals.idUrl = txtDomain.text;
          pref.addDomain(txtDomain.text);
//          Navigator.push(
//            context,
//            MaterialPageRoute(builder: (context) => MenuPage()),
//          );
          Navigator.push(context, MaterialPageRoute(builder: (context) =>LoginPage()),);
        }
      });
    } else {
      setState(() {
        isAutoValidation = false;
      });
      new ErrorDialog(context).setMessage("Domain not found").show();
    }
  }

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
      body: new Container(
        alignment: Alignment.center,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/bgopensys.jpg"),
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.3), BlendMode.dstATop))),
        child: ListView(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Center(
                child: new Image.asset(
                  "assets/opinsyslogo.png",
                  width: _style.widthScreen() / 1.8,
//                  height: _style.widthScreen() / 2.5,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10, left: 30, right: 30),
              child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextFormField(
                        controller: txtDomain,
                        autovalidate: isAutoValidation,
                        cursorColor: Colors.teal,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          filled: true,
                          labelText: 'School Domain ',
                          fillColor: Colors.white.withOpacity(0.2),
                          labelStyle: TextStyle(color: Colors.teal),
                          errorStyle: TextStyle(color: Color(0xFFFF7777)),
                          border: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.teal)),
                          enabledBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.teal)),
                          errorBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.red)),
                          focusedBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.teal)),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter School Domain';
                          }
                          return null;
                        },
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 20.0),
                        height: 50.0,
                        width: 400.0,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: FlatButton(
                          color: Colors.orange,
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.navigate_next,
                                  color: Colors.white,
                                ),
                                Container(
                                  child: Text(
                                    "Next",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  padding: EdgeInsets.symmetric(horizontal: 5),
                                )
                              ]),
                          onPressed: () {
                            check("login");
                          },
                        ),
                      ),
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }
}
