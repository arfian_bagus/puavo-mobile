import 'dart:async';

import 'package:flutter/material.dart';
import 'package:puavomobile/pages/splashscreen.dart';
import 'package:puavomobile/style/style.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:puavomobile/services/globals.dart' as globals;
import 'package:puavomobile/pages/login.dart';

import 'domain.dart';
import 'loading.dart';

class Dashboard extends StatefulWidget {
  String domain;

  Dashboard({Key key, this.domain});
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  String title = "Home";
  final appTitle = 'Opinsys';
  WebViewController _webViewController;
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  bool isLoad;
  Style _style;
  String idSchool = "";
  String urlLoad;

  @override
  void initState() {
    globals.setContext(context);
    isLoad = true;
    urlLoad = "https://" + widget.domain + ".opinsys.fi/users/schools";
    super.initState();
  }

  void changeUrl(String url) {
    setState(() {
      urlLoad = url;
      isLoad = true;
    });
    _webViewController.loadUrl(url).whenComplete(() {
      setState(() {
        isLoad = false;
      });
    });
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        backgroundColor: Colors.orange,
      ),
      body: Stack(
        children: <Widget>[
          Builder(builder: (BuildContext context) {
            return Container(
                child: Stack(children: <Widget>[
              Positioned(
                width: _style.widthScreen(),
                height: _style.heightScreen() - AppBar().preferredSize.height,
//                top: _style.widthScreen() * 0.59 * -1,
                child: WebView(
                  initialUrl: urlLoad,
                  javascriptMode: JavascriptMode.unrestricted,
                  onWebViewCreated: (WebViewController webViewController) {
                    _webViewController = webViewController;
                    _controller.complete(webViewController);
                    if (_controller.isCompleted) {
                      setState(() {
                        isLoad = false;
                      });
                    }
                  },
                  javascriptChannels: <JavascriptChannel>[
                    _toasterJavascriptChannel(context),
                  ].toSet(),
                  navigationDelegate: (NavigationRequest request) {
//                  if (request.url.startsWith('https://www.youtube.com/')) {
//                    return NavigationDecision.prevent;
//                  }
                    return NavigationDecision.navigate;
                  },
                  onPageStarted: (String url) {
                    print("start url " + url);
                  },
                  onPageFinished: (String url) {
                    _webViewController.loadUrl("javascript:(function() { " +
                        "var topbarWrapper = document.getElementById('topbarWrapper').style.display='none'; " +
                        "var schoolHeader = document.getElementById('schoolHeader').style.display='none'; " +
                        "var tabs = document.getElementById('tabs').style.display='none'; " +
                        "})()");
                    print("end url " + url);
//                    print((_style.heightScreen() * 0.28));
                    if (url ==
                        "https://" +
                            widget.domain +
                            ".opinsys.fi/users/logout")
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => SplashScreen()),
                      );

                    if (url ==
                        "https://" + widget.domain + ".opinsys.fi/users/login")
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => DomainPage()),
                      );

                    if (url.startsWith("https://" +
                        widget.domain +
                        ".opinsys.fi/users/schools")) {
                      var path = url.split("/");
                      idSchool = path[(path.length - 1)];
                    }
                  },
                ),
              ),
            ]));
          }),
          Visibility(
            visible: isLoad,
            child: LoadingDialogWidget(),
          ),
        ],
      ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
//              child: Text('Drawer Header'),
              decoration: BoxDecoration(
                  image: new DecorationImage(
                image: new AssetImage("assets/opinsyslogo.png"),
//                  fit: BoxFit.cover,
              )),
            ),
            ListTile(
              title: Row(
                  children: <Widget>[
                    Icon(Icons.home, color: Colors.teal,),
                    Container( child:Text("Home", style: TextStyle(color:Colors.teal),), padding: EdgeInsets.symmetric(horizontal: 5), )
                  ]),
              onTap: () {
                setState(() {
                  title = "Home";
                });
                changeUrl(
                    "https://" + widget.domain + ".opinsys.fi/users/schools/");
              },
            ),
            ListTile(
              title: Row(
                  children: <Widget>[
                    Icon(Icons.person, color: Colors.teal,),
                    Container( child:Text("Users", style: TextStyle(color:Colors.teal),), padding: EdgeInsets.symmetric(horizontal: 5), )
                  ]),
              onTap: () {
                setState(() {
                  title = "Users";
                });
                changeUrl("https://" +
                    widget.domain +
                    ".opinsys.fi/users/$idSchool/users");
              },
            ),
            ListTile(
              title: Row(
                  children: <Widget>[
                    Icon(Icons.people, color: Colors.teal,),
                    Container( child:Text("User Groups", style: TextStyle(color:Colors.teal),), padding: EdgeInsets.symmetric(horizontal: 5), )
                  ]),
              onTap: () {
                setState(() {
                  title = "User Groups";
                });
                changeUrl("https://" +
                    widget.domain +
                    ".opinsys.fi/users/$idSchool/groups");
              },
            ),
            ListTile(
              title: Row(
                  children: <Widget>[
                    Icon(Icons.verified_user, color: Colors.teal,),
                    Container( child:Text("Username Lists", style: TextStyle(color:Colors.teal),), padding: EdgeInsets.symmetric(horizontal: 5), )
                  ]),
              onTap: () {
                setState(() {
                  title = "Username Lists";
                });
                changeUrl("https://" +
                    widget.domain +
                    ".opinsys.fi/users/$idSchool/lists");
              },
            ),
            ListTile(
              title: Row(
                  children: <Widget>[
                    Icon(Icons.devices, color: Colors.teal,),
                    Container( child:Text("Devices", style: TextStyle(color:Colors.teal),), padding: EdgeInsets.symmetric(horizontal: 5), )
                  ]),
              onTap: () {
                setState(() {
                  title = "Devices";
                });
                changeUrl("https://" +
                    widget.domain +
                    ".opinsys.fi/devices/$idSchool/devices");
              },
            ),
            ListTile(
              title: Row(
                  children: <Widget>[
                    Icon(Icons.devices_other, color: Colors.teal,),
                    Container( child:Text("Device Statistics", style: TextStyle(color:Colors.teal),), padding: EdgeInsets.symmetric(horizontal: 5), )
                  ]),
              onTap: () {
                setState(() {
                  title = "Device Statistics";
                });
                changeUrl("https://" +
                    widget.domain +
                    ".opinsys.fi/devices/$idSchool/devices/device_statistics");
              },
            ),
            ListTile(
              title: Row(
                  children: <Widget>[
                    Icon(Icons.print, color: Colors.teal,),
                    Container( child:Text("Printing Permissions", style: TextStyle(color:Colors.teal),), padding: EdgeInsets.symmetric(horizontal: 5), )
                  ]),
              onTap: () {
                setState(() {
                  title = "Printing Permissions";
                });
                changeUrl("https://" +
                    widget.domain +
                    ".opinsys.fi/users/$idSchool/printer_permissions");
              },
            ),
            ListTile(
              title: Row(
                  children: <Widget>[
                    Icon(Icons.settings, color: Colors.teal,),
                    Container( child:Text("External Services", style: TextStyle(color:Colors.teal),), padding: EdgeInsets.symmetric(horizontal: 5), )
                  ]),
              onTap: () {
                setState(() {
                  title = "External Services";
                });
                changeUrl("https://" +
                    widget.domain +
                    ".opinsys.fi/users/schools/$idSchool/external_services");
              },
            ),
            ListTile(
              title: Row(
                  children: <Widget>[
                    Icon(Icons.refresh, color: Colors.teal,),
                    Container( child:Text("Change Password", style: TextStyle(color:Colors.teal),), padding: EdgeInsets.symmetric(horizontal: 5), )
                  ]),
              onTap: () {
                setState(() {
                  title = "Change Password";
                });
                changeUrl("https://" +
                    widget.domain +
                    ".opinsys.fi/users/password");
              },
            ),
            ListTile(
              title: Row(
                  children: <Widget>[
                    Icon(Icons.remove_from_queue, color: Colors.teal,),
                    Container( child:Text("Info Tv", style: TextStyle(color:Colors.teal),), padding: EdgeInsets.symmetric(horizontal: 5), )
                  ]),
              onTap: () {
                setState(() {
                  title = "Info Tv";
                });
                changeUrl("https://"+widget.domain+".opinsys.fi/infotv");
              },
            ),
            ListTile(
              title: Row(
                  children: <Widget>[
                    Icon(Icons.lock, color: Colors.teal,),
                    Container( child:Text("Logout", style: TextStyle(color:Colors.teal),), padding: EdgeInsets.symmetric(horizontal: 5), )
                  ]),
              onTap: () {
                changeUrl(
                    "https://" + widget.domain + ".opinsys.fi/users/logout");
              },
            ),
          ],
        ),
      ),
    );
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }
}
