import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puavomobile/models/usermodel.dart';
import 'package:puavomobile/pages/userdetail.dart';
import 'package:puavomobile/services/globals.dart' as globals;
import 'package:puavomobile/style/circle_loading.dart';
import 'package:puavomobile/style/error_dialog.dart';
import 'package:puavomobile/style/style.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:puavomobile/services/helper.dart' as helper;
import 'package:puavomobile/services/user_api.dart' as user_api;

class UserPage extends StatefulWidget {
  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage>
    with SingleTickerProviderStateMixin {
  Style _style;
  bool isLoad;
  ScrollController _controller = new ScrollController();
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  List<UserModel> listUser = new List();
  List<UserGroupModel> listGroup = new List();
  TabController _controllerTab;
  int currentIndex = 0;

  @override
  void initState() {
    globals.setContext(context);
    isLoad = false;
    currentIndex = 0;
    _controllerTab = new TabController(length: 2, vsync: this);
    refreshData();
    super.initState();
  }

  void onRefresh() async {
    await Future.delayed(Duration(milliseconds: 500));

    refreshData();
    _refreshController.refreshCompleted();
    _refreshController.loadComplete();
  }

  Future<void> refreshData() async {
    setState(() {
      isLoad = true;
    });
    user_api.getUsers().then((res) {
      if (res.length > 0) {
        setState(() {
          listUser = res;
        });
      }
      setState(() {
        isLoad = false;
      });
    }).catchError((message) {
      if (message != "error connection")
        new ErrorDialog(context).setMessage(message).show();

      setState(() {
        isLoad = false;
      });
    });

    user_api.getGroups().then((res) {
      if (res.length > 0) {
        setState(() {
          listGroup = res;
//          isLoad = false;
        });
      }
    }).catchError((message) {
      if (message != "error connection")
        new ErrorDialog(context).setMessage(message).show();

      setState(() {
//        isLoad = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
        body: new Container(
            width: _style.widthScreen(),
//            height: _style.heightScreen(),
            child: Stack(children: <Widget>[
              isLoad
                  ? WidgetCircleLoading()
                  : new Container(
                      child: new Container(
                          color: Colors.white,
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: new SmartRefresher(
                              enablePullDown: true,
                              enablePullUp: false,
                              header: MaterialClassicHeader(),
                              controller: _refreshController,
                              onRefresh: onRefresh,
                              child: new CustomScrollView(
                                  controller: _controller,
                                  physics: AlwaysScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  slivers: <Widget>[
                                    SliverList(
                                        delegate: SliverChildListDelegate([
                                      new Container(
                                          child: new Column(
                                        children: <Widget>[
                                          new Container(
                                            height: 80,
                                            child: new TabBar(
                                              indicatorColor: Colors.teal,
                                              labelColor: Colors.teal,
                                              unselectedLabelColor:
                                                  Colors.black,
                                              controller: _controllerTab,
                                              onTap: (index) {
                                                setState(() {
                                                  currentIndex = index;
                                                });
                                              },
                                              tabs: [
                                                new Tab(
                                                  icon: Icon(
                                                    Icons.person,
                                                    color: currentIndex == 0
                                                        ? Colors.teal
                                                        : Colors.black,
                                                    size: 40,
                                                  ),
                                                  text: 'Users',
                                                ),
                                                new Tab(
                                                  icon: Icon(Icons.people,
                                                      color: currentIndex == 1
                                                          ? Colors.teal
                                                          : Colors.black,
                                                      size: 40),
                                                  text: 'User Groups',
                                                ),
                                              ],
                                            ),
                                          ),
                                          new Container(
//                                                padding: EdgeInsets.only(bottom: 100),
                                            height:
                                                (_style.heightScreen() - 180),
                                            child: new TabBarView(
                                              controller: _controllerTab,
                                              children: <Widget>[
                                                new Container(
                                                  child: new ListView.builder(
                                                      scrollDirection:
                                                          Axis.vertical,
                                                      physics:
                                                          ClampingScrollPhysics(),
                                                      shrinkWrap: true,
                                                      itemCount:
                                                          listUser.length,
                                                      itemBuilder:
                                                          (context, index) {
                                                        return GestureDetector(
                                                            onTap: () {
                                                              Navigator.push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                    builder:
                                                                        (context) =>
                                                                            DetailUserPage(
                                                                      user: listUser[
                                                                          index],
                                                                    ),
                                                                  ));
                                                            },
                                                            child: Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            20,
                                                                        right:
                                                                            20,
                                                                        bottom:
                                                                            10),
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        top:
                                                                            10),
                                                                decoration:
                                                                    BoxDecoration(
                                                                  border: Border(
                                                                      top: BorderSide(
                                                                          color: index == 0
                                                                              ? Colors.white
                                                                              : Colors.grey[300],
                                                                          width: 1)),
                                                                ),
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: <
                                                                      Widget>[
//                                                                      Row(children: <Widget>[
//                                                                        Text("Test", style: TextStyle(fontWeight : FontWeight.bold, fontSize: 20),),
//                                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text("Text"),))
//                                                                      ]),
                                                                    Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .end,
                                                                      children: <
                                                                          Widget>[
                                                                        ClipOval(
                                                                          child:
                                                                              Container(
                                                                            color: index % 2 == 0
                                                                                ? Colors.orange
                                                                                : Colors.teal,
                                                                            width:
                                                                                50,
                                                                            height:
                                                                                50,
                                                                            child: Container(
                                                                                margin: EdgeInsets.only(top: 15, left: 15),
                                                                                child: Text(
                                                                                  helper.inisial(listUser[index].reverse_name.length>0?listUser[index].reverse_name:listUser[index].first_name),
                                                                                  style: TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),
                                                                                )),
                                                                          ),
                                                                        ),
                                                                        Expanded(
                                                                            child:
                                                                                Container(
                                                                          margin:
                                                                              EdgeInsets.only(left: 10),
                                                                          child:
                                                                              Column(
                                                                            children: <Widget>[
                                                                              Row(
                                                                                children: <Widget>[
                                                                                  Text(
                                                                                    listUser[index].reverse_name.length>0?listUser[index].reverse_name:listUser[index].first_name,
                                                                                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                                                                                  ),
//                                                                                    Expanded( child: Align(alignment: Alignment.centerRight, child: Text(listUser[index].reverse_name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),)))
                                                                                ],
                                                                              ),
                                                                              Row(
                                                                                children: <Widget>[
                                                                                  Text(listUser[index].username),
                                                                                  Expanded(
                                                                                      child: Align(
                                                                                    alignment: Alignment.centerRight,
                                                                                    child: Text(listUser[index].user_type),
                                                                                  ))
                                                                                ],
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ))
                                                                      ],
                                                                    ),
                                                                  ],
                                                                )));
                                                      }),
                                                ),
                                                new Container(
                                                  child: new ListView.builder(
                                                      scrollDirection:
                                                          Axis.vertical,
                                                      physics:
                                                          ClampingScrollPhysics(),
                                                      shrinkWrap: true,
                                                      itemCount:
                                                          listGroup.length,
                                                      itemBuilder:
                                                          (context, index) {
                                                        return Card(
                                                            margin:
                                                                EdgeInsets.all(
                                                                    10),
                                                            child:
                                                                ExpandablePanel(
                                                              theme:
                                                                  const ExpandableThemeData(
                                                                headerAlignment:
                                                                    ExpandablePanelHeaderAlignment
                                                                        .center,
                                                                tapBodyToExpand:
                                                                    true,
                                                                tapBodyToCollapse:
                                                                    true,
                                                              ),
                                                              header: Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            15,
                                                                        top: 8),
                                                                child: Text(
                                                                  listGroup[
                                                                          index]
                                                                      .name,
                                                                  style: _style
                                                                      .fontStyleH7,
                                                                ),
                                                              ),
//                                                                collapsed: Text(listGroup[index].type, softWrap: true, maxLines: 2, overflow: TextOverflow.ellipsis,),
                                                              expanded: new Container(
                                                                  child: new ListView.builder(
                                                                      scrollDirection: Axis.vertical,
                                                                      physics: ClampingScrollPhysics(),
                                                                      shrinkWrap: true,
                                                                      itemCount: listGroup[index].member_usernames.length,
                                                                      itemBuilder: (context, index2) {
                                                                        return Container(
                                                                          margin:
                                                                              EdgeInsets.only(left: 15),
                                                                          child:
                                                                              Row(
                                                                            children: <Widget>[
                                                                              Container(
                                                                                padding: EdgeInsets.all(3),
                                                                                child: Icon(
                                                                                  Icons.play_arrow,
                                                                                  color: Colors.orange,
                                                                                ),
                                                                              ),
                                                                              Expanded(
                                                                                child: GestureDetector(
                                                                                    onTap: () {
                                                                                      var user = listUser.where((user) => user.username.contains(listGroup[index].member_usernames[index2])).toList();
                                                                                      Navigator.push(
                                                                                          context,
                                                                                          MaterialPageRoute(
                                                                                            builder: (context) => DetailUserPage(
                                                                                              user: user[0],
                                                                                            ),
                                                                                          ));
                                                                                    },
                                                                                    child: Text(
                                                                                      listGroup[index].member_usernames[index2],
                                                                                      style: _style.fontStyleH7,
                                                                                    )),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        );
                                                                      })),
                                                            ));
                                                      }),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ))
                                    ]))
                                  ]))))
            ])));
  }
}
