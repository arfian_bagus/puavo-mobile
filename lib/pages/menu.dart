import 'package:flutter/material.dart';
import 'package:puavomobile/style/style.dart';
import 'package:puavomobile/pages/login.dart';
import 'package:puavomobile/services/globals.dart' as globals;

import 'changepass.dart';

class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  Style _style;
  bool isAutoValidation = false;

  @override
  void initState() {
    globals.setContext(context);
    super.initState();
  }

  Future check(String type) async {
    if (type == "login")
      Navigator.push(context, MaterialPageRoute(builder: (context) =>LoginPage()),);
    else if (type == "change")
      Navigator.push(context, MaterialPageRoute(builder: (context) =>ChangePass(domain: globals.idUrl, url: "https://"+globals.idUrl+".opinsys.fi/users/password", title: "Change Password",)),);
    else
      Navigator.push(context, MaterialPageRoute(builder: (context) =>ChangePass(domain: globals.idUrl, url: "https://"+globals.idUrl+".opinsys.fi/infotv", title: "Info TV",)),);
  }

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
      body: new Container(
        alignment: Alignment.center,
        decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage("assets/bgopensys.jpg"),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop)
            ),
        ),
        child: ListView(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Center(
                child: new Image.asset(
                  "assets/opinsyslogo.png",
                  width: _style.widthScreen() / 1.8,
//                  height: _style.widthScreen() / 2.5,
                ),
              ),
            ),
            Center(child:Text("Welcome " + globals.idUrl, style: TextStyle(fontSize: 20),)),
            Container(
              margin: EdgeInsets.only(top: 10, left: 30, right: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    height: 50.0,
                    width: 400.0,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: FlatButton(
                      color: Colors.orange,
                      child: Row( // R
                        mainAxisAlignment: MainAxisAlignment.center,// eplace with a Row for horizontal icon + text
                        children: <Widget>[
                          Icon(Icons.lock_open, color: Colors.white,),
                          Container( child:Text("Log In", style: TextStyle(color:Colors.white),), padding: EdgeInsets.symmetric(horizontal: 5), )
                        ]),
                      onPressed: () {


                        check("login");

                      },
                    ),
                  ),

                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    height: 50.0,
                    width: 400.0,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: FlatButton(
                      color: Colors.orange,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.remove_from_queue, color: Colors.white,),
                            Container( child:Text("Info Tv", style: TextStyle(color:Colors.white),), padding: EdgeInsets.symmetric(horizontal: 5), )
                          ]),
                      onPressed: () {


//                        check("login");

                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    height: 50.0,
                    width: 400.0,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: FlatButton(
                      color: Colors.orange,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.refresh, color: Colors.white,),
                            Container( child:Text("Change Password", style: TextStyle(color:Colors.white),), padding: EdgeInsets.symmetric(horizontal: 5), )
                          ]),
                      onPressed: () {


                        check("change");

                      },
                    ),
                  ),
                ],
              ),
            )

          ],
        ),
      ),
    );
  }
}