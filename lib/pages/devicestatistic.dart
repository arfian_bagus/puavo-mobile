import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:puavomobile/models/devicemodel.dart';
import 'package:puavomobile/services/globals.dart' as globals;
import 'package:puavomobile/style/circle_loading.dart';
import 'package:puavomobile/style/error_dialog.dart';
import 'package:puavomobile/style/style.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:puavomobile/services/helper.dart' as helper;
import 'package:puavomobile/services/device_api.dart' as device_api;

import 'detaildevice.dart';

class DeviceStatisticPage extends StatefulWidget {
  @override
  _DeviceStatisticPageState createState() => _DeviceStatisticPageState();
}

class _DeviceStatisticPageState extends State<DeviceStatisticPage>
    with SingleTickerProviderStateMixin {
  Style _style;
  bool isLoad;
  ScrollController _controller = new ScrollController();
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
//  List<DeviceModel> listSession = new List();
//  List<DeviceModel> listBattery = new List();
  List<String> listGroup = new List();
  TabController _controllerTab;
  int currentIndex = 0;

  @override
  void initState() {
    globals.setContext(context);
    isLoad = false;
    currentIndex = 0;
    _controllerTab = new TabController(length: 2, vsync: this);
    refreshData();
    super.initState();
  }

  void onRefresh() async {
    await Future.delayed(Duration(milliseconds: 500));

    refreshData();
    _refreshController.refreshCompleted();
    _refreshController.loadComplete();
  }

  Future<void> refreshData() async {
    setState(() {
      isLoad = true;
    });

    device_api.getDeviceImages().then((res) {
      if (res.length > 0) {
        setState(() {
          listGroup = res.replaceAll('[', '').replaceAll(']', '').split(",").toList();
          isLoad = false;
        });
      }
    }).catchError((message) {
      if (message != "error connection")
        new ErrorDialog(context).setMessage(message).show();

      setState(() {
        isLoad = false;
      });
    });

//    device_api.getDevices().then((res) {
//      if (res.length > 0) {
//        setState(() {
//          res.sort((a, b) => a.hwobj.timestamp.compareTo(b.hwobj.timestamp));
//          listSession = res;
//          isLoad = false;
//        });
//      }
//    }).catchError((message) {
//      if (message != "error connection")
//        new ErrorDialog(context).setMessage(message).show();
//
//      setState(() {
//        isLoad = false;
//      });
//    });
//
//    device_api.getDevices().then((res) {
//      if (res.length > 0) {
//        setState(() {
//          for (var o in res) {
//            String btr = o.hwobj.battery["percentage"].toString();
//            o.percentageInt = double.parse(btr.substring(0, btr.length - 1));
//          }
//          res.sort((a, b) => a.percentageInt.compareTo(b.percentageInt));
//          listBattery = res;
//          isLoad = false;
//        });
//      }
//    }).catchError((message) {
//      if (message != "error connection")
//        new ErrorDialog(context).setMessage(message).show();
//
//      setState(() {
//        isLoad = false;
//      });
//    });
  }

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
        body: new Container(
            width: _style.widthScreen(),
//            height: _style.heightScreen(),
            child: Stack(children: <Widget>[
              isLoad
                  ? WidgetCircleLoading()
                  : new Container(
                      child: new Container(
                          color: Colors.white,
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: new SmartRefresher(
                              enablePullDown: true,
                              enablePullUp: false,
                              header: MaterialClassicHeader(),
                              controller: _refreshController,
                              onRefresh: onRefresh,
                              child: new CustomScrollView(
                                  controller: _controller,
                                  physics: AlwaysScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  slivers: <Widget>[
                                    SliverList(
                                        delegate: SliverChildListDelegate([
                                      new Container(
                                          child: new Column(
                                        children: <Widget>[
                                          new Container(
                                            child: new ListView.builder(
                                                scrollDirection: Axis.vertical,
                                                physics:
                                                    ClampingScrollPhysics(),
                                                shrinkWrap: true,
                                                itemCount: listGroup.length,
                                                itemBuilder: (context, index) {
                                                  return Card( child:Container(
                                                    padding: EdgeInsets.all(10),
                                                    child: new Column(
                                                      children: <Widget>[
                                                        new Text(listGroup[index], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                                                        SizedBox(height: 5),
                                                        new Container(
                                                          child: new LinearPercentIndicator(
                                                            width: _style.widthScreen() - 50,
                                                            animation: true,
                                                            lineHeight: 20.0,
                                                            animationDuration: 2500,
                                                            percent: 1,
                                                            center: Text("100.0%"),
                                                            linearStrokeCap: LinearStrokeCap.roundAll,
                                                            progressColor: Colors.green,
                                                          ),
                                                        ),
                                                      ],
                                                    )),
                                                  );
                                                }),
                                          ),

//                                              new Container(
//                                                height: 80,
//                                                child: new TabBar(
//                                                  indicatorColor: Colors.teal,
//                                                  labelColor: Colors.teal,
//                                                  unselectedLabelColor: Colors.black,
//                                                  controller: _controllerTab,
//                                                  onTap: (index){
//                                                    setState(() {
//                                                      currentIndex = index;
//                                                    });
//                                                  },
//                                                  tabs: [
//                                                    new Tab(
//                                                      icon: Icon(Icons.timer, color: currentIndex == 0 ? Colors.teal : Colors.black, size: 40),
//                                                      text: 'Last Open',
//                                                    ),
//                                                    new Tab(
//                                                      icon: Icon(Icons.battery_alert, color: currentIndex == 1 ? Colors.teal : Colors.black, size: 40),
//                                                      text: 'Critical Battery',
//                                                    ),
//                                                  ],
//                                                ),
//                                              ),
//
//                                              new Container(
////                                                padding: EdgeInsets.only(bottom: 100),
//                                                height: (_style.heightScreen() - 180),
//                                                child: new TabBarView(
//                                                  controller: _controllerTab,
//                                                  children: <Widget>[
//                                                    new Container(
//                                                      child: new ListView.builder(
//                                                          scrollDirection: Axis.vertical,
//                                                          physics: ClampingScrollPhysics(),
//                                                          shrinkWrap: true,
//                                                          itemCount: listSession.length,
//                                                          itemBuilder: (context,index){
//                                                            return GestureDetector(onTap: (){
//                                                              Navigator.push(
//                                                                context,
//                                                                MaterialPageRoute(
//                                                                    builder: (context) => DetailDevicePage(device: listSession[index],),
//                                                              ));
//                                                            }, child: Container(
//                                                                margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
//                                                                padding: EdgeInsets.only(top: 10),
//                                                                decoration: BoxDecoration(
//                                                                  border: Border(
//                                                                      top: BorderSide(
//                                                                          color: index == 0 ? Colors.white : Colors.grey[300], width: 1)),
//                                                                ),
//                                                                child: Column(
//                                                                  crossAxisAlignment: CrossAxisAlignment.start,
//                                                                  children: <Widget>[
////                                                                      Row(children: <Widget>[
////                                                                        Text("Test", style: TextStyle(fontWeight : FontWeight.bold, fontSize: 20),),
////                                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text("Text"),))
////                                                                      ]),
//                                                                    Row(
//                                                                      mainAxisAlignment: MainAxisAlignment.end,
//                                                                      children: <Widget>[
//                                                                        ClipOval(
//                                                                          child: Container(
//                                                                            color: index%2==0 ? Colors.orange : Colors.teal,
//                                                                            width: 50,
//                                                                            height: 50,
//                                                                            child: Container(
//                                                                              child: new Icon(Icons.computer, color: Colors.white, size: 30,),),
//                                                                          ),
//                                                                        ),
//                                                                        Expanded(
//                                                                            child: Container(
//                                                                              margin: EdgeInsets.only(left: 10),
//                                                                              child: Column(children: <Widget>[
//                                                                                Row(children: <Widget>[
//                                                                                  Text(listSession[index].hostname, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
////                                                                                    Expanded( child: Align(alignment: Alignment.centerRight, child: Text(listUser[index].reverse_name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),)))
//                                                                                ],),
//                                                                                Row(children: <Widget>[
//                                                                                  Text("Last Open "+helper.timeStampToDate(listSession[index].hwobj.timestamp)),
////                                                                                  Expanded( child: Align(alignment: Alignment.centerRight, child: Text(listUser[index].user_type),))
//                                                                                ],),
//                                                                              ],),
//                                                                            )
//                                                                        )
//                                                                      ],),
//                                                                  ],)
//                                                            ),);}
//                                                      ),
//                                                    ),
//                                                    new Container(
//                                                      child: new ListView.builder(
//                                                          scrollDirection: Axis.vertical,
//                                                          physics: ClampingScrollPhysics(),
//                                                          shrinkWrap: true,
//                                                          itemCount: listBattery.length,
//                                                          itemBuilder: (context,index){
//                                                            return GestureDetector(onTap: (){
//                                                              Navigator.push(
//                                                                  context,
//                                                                  MaterialPageRoute(
//                                                                    builder: (context) => DetailDevicePage(device: listBattery[index],),
//                                                                  ));
//                                                            }, child: Container(
//                                                                margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
//                                                                padding: EdgeInsets.only(top: 10),
//                                                                decoration: BoxDecoration(
//                                                                  border: Border(
//                                                                      top: BorderSide(
//                                                                          color: index == 0 ? Colors.white : Colors.grey[300], width: 1)),
//                                                                ),
//                                                                child: Column(
//                                                                  crossAxisAlignment: CrossAxisAlignment.start,
//                                                                  children: <Widget>[
////                                                                      Row(children: <Widget>[
////                                                                        Text("Test", style: TextStyle(fontWeight : FontWeight.bold, fontSize: 20),),
////                                                                        Expanded( child: Align(alignment: Alignment.centerRight, child: Text("Text"),))
////                                                                      ]),
//                                                                    Row(
//                                                                      mainAxisAlignment: MainAxisAlignment.end,
//                                                                      children: <Widget>[
//                                                                        ClipOval(
//                                                                          child: Container(
//                                                                            color: index%2==0 ? Colors.orange : Colors.teal,
//                                                                            width: 50,
//                                                                            height: 50,
//                                                                            child: Container(
//                                                                              child: new Icon(Icons.computer, color: Colors.white, size: 30,),),
//                                                                          ),
//                                                                        ),
//                                                                        Expanded(
//                                                                            child: Container(
//                                                                              margin: EdgeInsets.only(left: 10),
//                                                                              child: Column(children: <Widget>[
//                                                                                Row(children: <Widget>[
//                                                                                  Text(listBattery[index].hostname, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
////                                                                                    Expanded( child: Align(alignment: Alignment.centerRight, child: Text(listUser[index].reverse_name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),)))
//                                                                                ],),
//                                                                                Row(children: <Widget>[
//                                                                                  Text("Battery "+listBattery[index].hwobj.battery["percentage"]),
////                                                                                  Expanded( child: Align(alignment: Alignment.centerRight, child: Text(listUser[index].user_type),))
//                                                                                ],),
//                                                                              ],),
//                                                                            )
//                                                                        )
//                                                                      ],),
//                                                                  ],)
//                                                            ));}
//                                                      ),
//                                                    ),
//                                                  ],
//                                                ),
//                                              ),
                                        ],
                                      ))
                                    ]))
                                  ]))))
            ])));
  }
}
