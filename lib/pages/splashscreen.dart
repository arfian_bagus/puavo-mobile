import 'dart:async';

import 'package:flutter/material.dart';
import 'package:puavomobile/services/shared_pref.dart' as pref;
import 'domain.dart';
import 'drawerpage.dart';
import 'homepage.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    loadData();
    super.initState();
  }

  getUsername() async {
    var temp = await pref.getUsername();
    print("========== pref =========");
    print(temp);
    if (temp == null) {
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => DomainPage()));
    } else {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => DrawerPage()),
      );
    }
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 2), onDoneLoading);
  }

  onDoneLoading() async {
//     Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>DomainPage()),);
    getUsername();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage("assets/bgopensys.jpg"),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop)
            )
        ),
        child: Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Image.asset(
                  "assets/opinsyslogo.png",
                  width: 250,
                ),
                Text(
                  "Mobile",
                  style: TextStyle(
                      fontFamily: "RobotoMono", fontSize: 32.0, fontWeight: FontWeight.bold
                  ),
                ),
              ],
            )
        ),
      ),
    );
  }
}