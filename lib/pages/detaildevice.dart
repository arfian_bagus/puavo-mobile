import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puavomobile/services/globals.dart' as globals;
import 'package:puavomobile/style/circle_loading.dart';
import 'package:puavomobile/style/style.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:puavomobile/models/devicemodel.dart';
import 'package:puavomobile/services/helper.dart' as helper;

class DetailDevicePage extends StatefulWidget {
  DeviceModel device;
  DetailDevicePage({Key key, this.device});

  @override
  _DetailDevicePageState createState() => _DetailDevicePageState();
}

class _DetailDevicePageState extends State<DetailDevicePage> {
  Style _style;
  bool isLoad;
  ScrollController _controller = new ScrollController();
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    globals.setContext(context);
    isLoad = false;
    super.initState();
  }

  void onRefresh() async {
    await Future.delayed(Duration(milliseconds: 500));

    refreshData();
    _refreshController.refreshCompleted();
    _refreshController.loadComplete();
  }

  Future<void> refreshData() async {}

  @override
  Widget build(BuildContext context) {
    _style = new Style(MediaQuery.of(context).size);

    return Scaffold(
        appBar: AppBar(
          title: Text("Detail Device"),
          backgroundColor: Colors.orange,
          centerTitle: true,
        ),
        body: new Container(
            width: _style.widthScreen(),
            height: _style.heightScreen(),
            child: Stack(children: <Widget>[
              isLoad
                  ? WidgetCircleLoading()
                  : new Container(
                      child: new Container(
                          color: Colors.white,
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: new SmartRefresher(
                              enablePullDown: true,
                              enablePullUp: true,
                              header: MaterialClassicHeader(),
                              footer: CustomFooter(
                                builder:
                                    (BuildContext context, LoadStatus mode) {
                                  Widget body;
                                  if (mode == LoadStatus.idle) {
                                    body = Container();
                                  } else if (mode == LoadStatus.loading) {
                                    body = CupertinoActivityIndicator();
                                  } else if (mode == LoadStatus.failed) {
                                    body = Container();
                                  } else if (mode == LoadStatus.canLoading) {
                                    body = Container();
                                  } else {
                                    body = Container();
                                  }
                                  return Container(
                                    height: 55.0,
                                    child: Center(child: body),
                                  );
                                },
                              ),
                              controller: _refreshController,
                              onRefresh: onRefresh,
                              child: new CustomScrollView(
                                  controller: _controller,
                                  physics: AlwaysScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  slivers: <Widget>[
                                    SliverList(
                                        delegate: SliverChildListDelegate([
                                      new Container(
                                          child: new Column(
                                        children: <Widget>[
                                          new Container(
                                            margin: EdgeInsets.only(top: 10),
                                            child: Center(
                                              child: ClipOval(
                                                child: Container(
                                                  color: Colors.teal,
                                                  width: 100,
                                                  height: 100,
                                                  child: Container(
                                                    child: new Icon(
                                                      Icons.computer,
                                                      color: Colors.white,
                                                      size: 60,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          new Container(
                                              margin:
                                                  EdgeInsets.only(bottom: 10),
                                              child: new Center(
                                                  child: new Text(
                                                widget.device.hostname,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 25),
                                              ))),
                                          _style.widgetVerticalShadow(
                                              title: "General Information",
                                              space: 5,
                                              child: new Container(
                                                  width: _style.widthScreen(),
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal:
                                                          _style.paddingLeft()),
                                                  child: new Column(
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Type :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget.device
                                                                    .type,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Mac Addresses :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                        ],
                                                      ),
                                                      Row(
                                                        children: <Widget>[
                                                          Container(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 25),
                                                            child: Text(
                                                                widget.device
                                                                    .mac_addresses
                                                                    .toString()
                                                                    .replaceAll(
                                                                        '[', '')
                                                                    .replaceAll(
                                                                        ']',
                                                                        ''),
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ),
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Available Images :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                        ],
                                                      ),
                                                      Row(
                                                        children: <Widget>[
                                                          Container(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 25),
                                                          ),
                                                          Expanded(
                                                            child: Text(
                                                                widget.device
                                                                    .available_images
                                                                    .toString()
                                                                    .replaceAll(
                                                                        '[', '')
                                                                    .replaceAll(
                                                                        ']',
                                                                        ''),
                                                                style: _style
                                                                    .fontStyleH7),
                                                          )
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Keyboard Layout : ",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget.device
                                                                    .keyboard_layout,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Timezone :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget.device
                                                                    .timezone,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Model :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget.device
                                                                    .model,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Preferred Language :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget.device
                                                                    .preferred_language,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                    ],
                                                  ))),
                                          _style.widgetVerticalShadow(
                                              title: "Detail Information",
                                              space: 5,
                                              child: new Container(
                                                  width: _style.widthScreen(),
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal:
                                                          _style.paddingLeft()),
                                                  child: new Column(
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Last Open :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                helper.timeStampToDate(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .timestamp),
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Kernel Release :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget
                                                                    .device
                                                                    .hwobj
                                                                    .kernelrelease,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Bios Vendor :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget
                                                                    .device
                                                                    .hwobj
                                                                    .bios_vendor,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Bios Version :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget
                                                                    .device
                                                                    .hwobj
                                                                    .bios_version,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Processor",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                        ],
                                                      ),
                                                      Row(
                                                        children: <Widget>[
                                                          Container(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 25),
                                                          ),
                                                          Expanded(
                                                            child: Text(
                                                                widget
                                                                    .device
                                                                    .hwobj
                                                                    .processor0,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          )
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Memory(Mb) :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget
                                                                    .device
                                                                    .hwobj
                                                                    .memorysize_mb,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Manufacturer :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget
                                                                    .device
                                                                    .hwobj
                                                                    .manufacturer,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Product Name :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget
                                                                    .device
                                                                    .hwobj
                                                                    .productname,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Serial Number :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget
                                                                    .device
                                                                    .hwobj
                                                                    .serialnumber,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Board Serial :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget
                                                                    .device
                                                                    .hwobj
                                                                    .boardserialnumber,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                                Colors.orange,
                                                          ),
                                                          Text(
                                                            "Ssd :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget.device
                                                                    .hwobj.ssd,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                            Colors.orange,
                                                          ),
                                                          Text(
                                                            "Wifi :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                        ],
                                                      ),
                                                      Row(
                                                        children: <Widget>[
                                                          Container(
                                                            padding:
                                                            EdgeInsets.only(
                                                                left: 25),
                                                          ),
                                                          Expanded(
                                                            child: Text(
                                                                widget
                                                                    .device
                                                                    .hwobj.wifi,
                                                                style: _style
                                                                    .fontStyleH7),
                                                          )
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                    ],
                                                  ))),
                                          _style.widgetVerticalShadow(
                                              title: "Battery",
                                              space: 5,
                                              child: new Container(
                                                  width: _style.widthScreen(),
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal:
                                                      _style.paddingLeft()),
                                                  child: new Column(
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                            Colors.orange,
                                                          ),
                                                          Text(
                                                            "Vendor :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .battery[
                                                                    "vendor"]
                                                                        .toString(),
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                            Colors.orange,
                                                          ),
                                                          Text(
                                                            "Model :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .battery[
                                                                    "model"]
                                                                        .toString(),
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                            Colors.orange,
                                                          ),
                                                          Text(
                                                            "Serial :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .battery[
                                                                    "serial"]
                                                                        .toString(),
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                            Colors.orange,
                                                          ),
                                                          Text(
                                                            "State :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .battery[
                                                                    "state"]
                                                                        .toString(),
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                            Colors.orange,
                                                          ),
                                                          Text(
                                                            "Warning-level :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .battery[
                                                                    "warning-level"]
                                                                        .toString(),
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                            Colors.orange,
                                                          ),
                                                          Text(
                                                            "Energy :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .battery[
                                                                    "energy"]
                                                                        .toString(),
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                            Colors.orange,
                                                          ),
                                                          Text(
                                                            "Energy Empty :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .battery[
                                                                    "energy-empty"]
                                                                        .toString(),
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                            Colors.orange,
                                                          ),
                                                          Text(
                                                            "Energy Full Design :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .battery[
                                                                    "energy-full-design"]
                                                                        .toString(),
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                            Colors.orange,
                                                          ),
                                                          Text(
                                                            "voltage :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .battery[
                                                                    "voltage"]
                                                                        .toString(),
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                            Colors.orange,
                                                          ),
                                                          Text(
                                                            "Percentage :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .battery[
                                                                    "percentage"]
                                                                        .toString(),
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                            Colors.orange,
                                                          ),
                                                          Text(
                                                            "Capacity :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .battery[
                                                                    "capacity"]
                                                                        .toString(),
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                      Row(
                                                        children: <Widget>[
                                                          Icon(
                                                            Icons.play_arrow,
                                                            color:
                                                            Colors.orange,
                                                          ),
                                                          Text(
                                                            "Technology :",
                                                            style: _style
                                                                .fontStyleH7,
                                                          ),
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .battery[
                                                                    "technology"]
                                                                        .toString(),
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                    ],
                                                  ))),
                                          _style.widgetVerticalShadow(
                                              title: "Lspci",
                                              space: 5,
                                              child: new Container(
                                                  width: _style.widthScreen(),
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal:
                                                          _style.paddingLeft()),
                                                  child: new Column(
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Expanded(
                                                              child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Text(
                                                                widget
                                                                    .device
                                                                    .hwobj
                                                                    .lspci_values
                                                                    .toString()
                                                                    .replaceAll(
                                                                        '[', '')
                                                                    .replaceAll(
                                                                        ']',
                                                                        '').replaceAll(
                                                                    ',', '\n'),
                                                                style: _style
                                                                    .fontStyleH7),
                                                          ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                    ],
                                                  ))),
                                          _style.widgetVerticalShadow(
                                              title: "Lsusb",
                                              space: 5,
                                              child: new Container(
                                                  width: _style.widthScreen(),
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal:
                                                      _style.paddingLeft()),
                                                  child: new Column(
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .lsusb_values
                                                                        .toString()
                                                                        .replaceAll(
                                                                        '[', '')
                                                                        .replaceAll(
                                                                        ']',
                                                                        '').replaceAll(
                                                                        ',', '\n'),
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                    ],
                                                  ))),
                                          _style.widgetVerticalShadow(
                                              title: "Xrandr",
                                              space: 5,
                                              child: new Container(
                                                  width: _style.widthScreen(),
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal:
                                                      _style.paddingLeft()),
                                                  child: new Column(
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Expanded(
                                                              child: Align(
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child: Text(
                                                                    widget
                                                                        .device
                                                                        .hwobj
                                                                        .xrandr,
                                                                    style: _style
                                                                        .fontStyleH7),
                                                              ))
                                                        ],
                                                      ),
                                                      SizedBox(height: 5),
                                                    ],
                                                  ))),
                                        ],
                                      ))
                                    ]))
                                  ]))))
            ])));
  }
}
