import 'package:flutter/material.dart';
import 'package:puavomobile/pages/splashscreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Opinsys',
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    );
  }
}