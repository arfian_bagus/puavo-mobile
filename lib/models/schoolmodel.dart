class SchoolModel {
  String dn;
  String id;
  String name;
  bool allow_guest;
  bool automatic_image_updates;
  bool personal_device;
  String abbreviation;
  String timezone;
  String keyboard_layout;
  List<String> member_dns;
  List<String> member_usernames;
  String puavo_id;
  String preferred_language;

  SchoolModel(
      {this.dn = "",
        this.id = "",
        this.name = "",
        this.allow_guest,
        this.automatic_image_updates,
        this.personal_device,
        this.abbreviation = "",
        this.timezone = "",
        this.keyboard_layout = "",
        this.member_dns,
        this.member_usernames,
        this.puavo_id = "",
        this.preferred_language = ""
      });

  SchoolModel.fromJson(Map<String, dynamic> json)
      : dn = json['dn']==null?"":json['dn'],
        id = json['id']==null?"":json['id'],
        name = json['name']==null?"":json['name'],
        allow_guest = json['allow_guest'],
        automatic_image_updates = json['automatic_image_updates'],
        personal_device = json['personal_device'],
        abbreviation = json['abbreviation']==null?"":json['abbreviation'],
        timezone = json['timezone']==null?"":json['timezone'],
        keyboard_layout = json['keyboard_layout']==null?"":json['keyboard_layout'],
        member_dns = new List<String>.from(json['member_dns']),
        member_usernames = new List<String>.from(json['member_usernames']),
        puavo_id = json['puavo_id']==null?"":json['puavo_id'],
        preferred_language = json['preferred_language']==null?"":json['preferred_language'];

  static List<SchoolModel> jsonToList(List data) {
    return data
        .map<SchoolModel>((data) => SchoolModel.fromJson(data))
        .toList();
  }
}

class SchoolAuthModel {
  String dn;
  String id;
  String name;

  SchoolAuthModel(
      {this.dn = "",
        this.id = "",
        this.name = "",
      });

  SchoolAuthModel.fromJson(Map<String, dynamic> json)
      : dn = json['dn'],
        id = json['id'],
        name = json['name'];

  static List<SchoolAuthModel> jsonToList(List data) {
    return data
        .map<SchoolAuthModel>((data) => SchoolAuthModel.fromJson(data))
        .toList();
  }
}