import 'dart:convert';

class DeviceModel {
  String dn;
  String mac_address;
  List<String> mac_addresses;
  List<String> available_images;
  bool automatic_image_updates;
  String preferred_boot_image;
  String type;
  String hostname;
  String puavo_id;
  String keyboard_layout;
  String timezone;
  bool allow_guest;
  String model;
  bool personal_device;
  String school_dn;
  String hw_info;
  String preferred_language;
  HwModel hwobj;
  double percentageInt;
  String note;

  DeviceModel(
      {this.dn = "",
        this.preferred_boot_image = "",
        this.mac_address = "",
        this.mac_addresses,
        this.available_images,
        this.automatic_image_updates,
        this.personal_device,
        this.type = "",
        this.hostname = "",
        this.timezone = "",
        this.keyboard_layout = "",
        this.allow_guest,
        this.school_dn = "",
        this.puavo_id = "",
        this.model = "",
        this.preferred_language = "",
        this.hw_info = "",
        this.hwobj,
        this.note,
        this.percentageInt = 0
      });

  DeviceModel.fromJson(Map<String, dynamic> json)
      : dn = json['dn']==null?"":json['dn'],
        preferred_boot_image = json['preferred_boot_image']==null?"":json['preferred_boot_image'],
        mac_address = json['mac_address']==null?"":json['mac_address'],
        allow_guest = json['allow_guest'],
        automatic_image_updates = json['automatic_image_updates'],
        personal_device = json['personal_device'],
        type = json["type"]==null?"":json['type'],
        hostname = json["hostname"]==null?"":json['hostname'],
        timezone = json['timezone']==null?"":json['timezone'],
        school_dn = json["school_dn"]==null?"":json['school_dn'],
        model = json["model"]==null?"":json['model'],
        hw_info = json["hw_info"]==null?"":json['hw_info'],
        keyboard_layout = json['keyboard_layout']==null?"":json['keyboard_layout'],
        mac_addresses = new List<String>.from(json['mac_addresses']),
        available_images = new List<String>.from(json['available_images']),
        puavo_id = json['puavo_id']==null?"":json['puavo_id'],
        hwobj = HwModel.fromJson(jsonDecode(json['hw_info'].toString())),
        preferred_language = json['preferred_language']==null?"":json['preferred_language'];

  static List<DeviceModel> jsonToList(List data) {
    return data
        .map<DeviceModel>((data) => DeviceModel.fromJson(data))
        .toList();
  }
}

class HwModel {
  int timestamp;
  String this_image;
  String this_release;
  String kernelrelease;
  String bios_vendor;
  String bios_version;
  String bios_release_date;
  String blockdevice_sda_model;
  int blockdevice_sda_size;
  int processorcount;
  String processor0;
  String memorysize_mb;
  String manufacturer;
  String productname;
  String serialnumber;
  String boardserialnumber;
  String ssd;
  List<Map<String, dynamic>> memory;
  List<String> lspci_values;
  List<String> lsusb_values;
  String wifi;
  String xrandr;
  Map<String, dynamic> battery;
  Map<String, dynamic> free_space;


  HwModel(
      {this.timestamp,
        this.this_image = "",
        this.this_release = "",
        this.kernelrelease = "",
        this.bios_vendor = "",
        this.bios_version = "",
        this.bios_release_date = "",
        this.blockdevice_sda_model = "",
        this.processorcount,
        this.processor0 = "",
        this.memorysize_mb = "",
        this.manufacturer = "",
        this.productname = "",
        this.serialnumber = "",
        this.boardserialnumber = "",
        this.ssd = "",
        this.wifi = "",
        this.xrandr = "",
        this.memory,
        this.battery,
        this.free_space,
        this.lspci_values,
        this.lsusb_values,
        this.blockdevice_sda_size
      });

  HwModel.fromJson(Map<String, dynamic> json)
      : timestamp = json['timestamp'],
        this_image = json['this_image']==null?"":json['this_image'],
        this_release = json['this_release']==null?"":json['this_release'],
        kernelrelease = json['kernelrelease']==null?"":json['kernelrelease'],
        bios_vendor = json['bios_vendor']==null?"":json['bios_vendor'],
        bios_version = json['bios_version']==null?"":json['bios_version'],
        bios_release_date = json["bios_release_date"]==null?"":json['bios_release_date'],
        blockdevice_sda_model = json["blockdevice_sda_model"]==null?"":json['blockdevice_sda_model'],
        processorcount = json['processorcount'],
        blockdevice_sda_size = json['blockdevice_sda_size'],
        processor0 = json["processor0"]==null?"":json['processor0'],
        memorysize_mb = json["memorysize_mb"]==null?"":json['memorysize_mb'],
        manufacturer = json["manufacturer"]==null?"":json['manufacturer'],
        productname = json['productname']==null?"":json['productname'],
        lspci_values = new List<String>.from(json['lspci_values']),
        lsusb_values = new List<String>.from(json['lsusb_values']),
        serialnumber = json['serialnumber']==null?"":json['serialnumber'],
        ssd = json['ssd']==null?"":json['ssd'],
        wifi = json['wifi']==null?"":json['wifi'],
        xrandr = json['xrandr']==null?"":json['xrandr'],
        memory = new List<Map<String, dynamic>>.from(json['memory']),
        battery = new Map<String, dynamic>.from(json['battery']),
        free_space = new Map<String, dynamic>.from(json['free_space']),
        boardserialnumber = json['boardserialnumber']==null?"":json['boardserialnumber'];

  static List<HwModel> jsonToList(List data) {
    return data
        .map<HwModel>((data) => HwModel.fromJson(data))
        .toList();
  }
}