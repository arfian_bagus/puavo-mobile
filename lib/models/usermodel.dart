class UserModel {
  String dn;
  int id;
  String username;
  String last_name;
  String first_name;
  String email;
  String preferred_language;
  String timezone;
  String edu_person_principal_name;
  String reverse_name;
  String domain_username;
  String user_type;
  String organisation_domain;
  int puavo_id;

  UserModel(
      {this.dn = "",
        this.id = 0,
        this.username = "",
        this.last_name = "",
        this.first_name = "",
        this.email = "",
        this.preferred_language = "",
        this.timezone = "",
        this.edu_person_principal_name = "",
        this.reverse_name = "",
        this.domain_username = "",
        this.user_type = "",
        this.organisation_domain = "",
        this.puavo_id = 0
      });

  UserModel.fromJson(Map<String, dynamic> json)
      : dn = json['dn']==null?"":json['dn'],
        id = json['id']==null?0:json['id'],
        last_name = json['last_name']==null?"":json['last_name'],
        first_name = json['first_name']==null?"":json['first_name'],
        email = json['email']==null?"":json['email'],
        preferred_language = json['preferred_language']==null?"":json['preferred_language'],
        edu_person_principal_name = json['edu_person_principal_name']==null?"":json['edu_person_principal_name'],
        timezone = json['timezone']==null?"":json['timezone'],
        reverse_name = json['reverse_name']==null?"":json['reverse_name'],
        domain_username = json['domain_username']==null?"":json['domain_username'],
        organisation_domain = json['organisation_domain']==null?"":json['organisation_domain'],
        puavo_id = json['puavo_id']==null?0:json['puavo_id'],
        username = json['username']==null?"":json['username'],
        user_type = json['user_type']==null?"":json['user_type'];

  static List<UserModel> jsonToList(List data) {
    return data
        .map<UserModel>((data) => UserModel.fromJson(data))
        .toList();
  }
}

class UserGroupModel {
  String dn;
  String id;
  String type;
  String abbreviation;
  String name;
  List<String> member_usernames;
  String school_id;

  UserGroupModel(
      {this.dn = "",
        this.id = "",
        this.type = "",
        this.abbreviation = "",
        this.name = "",
        this.member_usernames,
        this.school_id = ""
      });

  UserGroupModel.fromJson(Map<String, dynamic> json)
      : dn = json['dn'],
        id = json['id'],
        abbreviation = json["abbreviation"],
        name = json["name"],
        member_usernames = new List<String>.from(json['member_usernames']),
        school_id = json["school_id"],
        type = json['type'];

  static List<UserGroupModel> jsonToList(List data) {
    return data
        .map<UserGroupModel>((data) => UserGroupModel.fromJson(data))
        .toList();
  }
}