import 'dart:async';
import 'dart:convert';
import 'package:puavomobile/models/usermodel.dart';
import 'package:puavomobile/services/url_connect.dart' as url_connect;
import 'package:dio/dio.dart';
import 'package:puavomobile/services/shared_pref.dart' as pref;

Future<List<UserModel>> getUsers({
  Function refreshData
}) async {
  var school = await pref.getIdSchool();
  try {
    var dio = await url_connect.dio;
    Response<String> response =
    await dio.get("v3/schools/$school/users");
    return UserModel.jsonToList(jsonDecode(response.data));
  } on DioError catch (e) {
    if (e.response != null) {
      var json = jsonDecode(e.response.data);
      throw json["error"]["code"];
    } else {
      url_connect.internetError(e: e, loading: false, refreshData: refreshData);
      throw "error connection";
    }
  }
}

Future<List<UserGroupModel>> getGroups({
  Function refreshData
}) async {
  var school = await pref.getIdSchool();
  try {
    var dio = await url_connect.dio;
    Response<String> response =
    await dio.get("v3/schools/$school/groups");
    return UserGroupModel.jsonToList(jsonDecode(response.data));
  } on DioError catch (e) {
    if (e.response != null) {
      var json = jsonDecode(e.response.data);
      throw json["error"]["code"];
    } else {
      url_connect.internetError(e: e, loading: false, refreshData: refreshData);
      throw "error connection";
    }
  }
}