import 'dart:async';

import 'package:encrypt/encrypt.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

Future<String> getUsername() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("username");
}

Future<bool> deleteUsername() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.remove("username");
}

Future<String> addUsername(String username) async{
  try{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('username', username);
    return username;
  }catch(e){
    return null;
  }
}

Future<String> getNama() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("nama");
}

Future<bool> deleteNama() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.remove("nama");
}


Future<String> addNama(String nama) async{
  try{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('nama', nama);
    return nama;
  }catch(e){
    return null;
  }
}

Future<String> getPassword() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();

  final key = Key.fromUtf8('opinsysMobileSip');
  final iv = IV.fromLength(16);
  final encrypter = Encrypter(AES(key));
  final decrypted = encrypter.decrypt16(prefs.getString("password"), iv: iv);
  print("decrypted = "+decrypted);
  return decrypted;
}

Future<bool> deletePassword() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.remove("password");
}


Future<String> addPassword(String password) async{
  try{
    final key = Key.fromUtf8('opinsysMobileSip');
    final iv = IV.fromLength(16);
    final encrypter = Encrypter(AES(key));
    final encrypted = encrypter.encrypt(password, iv: iv);

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('password', encrypted.base16);
    print("encrypted = "+password);
    return password;
  }catch(e){
    return null;
  }
}

Future<String> getDomain() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("domain");
}

Future<bool> deleteDomain() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.remove("domain");
}


Future<String> addDomain(String domain) async{
  try{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('domain', domain);
    return domain;
  }catch(e){
    return null;
  }
}

Future<String> getIdSchool() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("idschool");
}

Future<bool> deleteIdSchool() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.remove("idschool");
}


Future<String> addIdSchool(String school) async{
  try{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('idschool', school);
    return school;
  }catch(e){
    return null;
  }
}

Future<String> getPhotoProfile() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("photoprofile");
}

Future<bool> deletePhotoProfile() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.remove("photoprofile");
}


Future<String> addPhotoProfile(String path) async{
  try{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('photoprofile', path);
    return path;
  }catch(e){
    return null;
  }
}

Future<String> getTimeZone() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString("timezone");
}

Future<bool> deleteTimeZone() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.remove("timezone");
}


Future<String> addTimeZone(String timezone) async{
  try{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('timezone', timezone);
    return timezone;
  }catch(e){
    return null;
  }
}