String inisial(String title){
  if (title.length > 0){
    var arrtitle = title.split(" ");
    if (arrtitle.length > 1){
      return arrtitle[0].substring(0,1).toUpperCase() + arrtitle[1].substring(0,1).toUpperCase();
    } else if (arrtitle.length == 1){
      return arrtitle[0].substring(0,2).toUpperCase();
    } else {
      return "";
    }
  } else {
    return "";
  }
}

String timeStampToDate(int timeStamp){
  DateTime dateTime = new DateTime.fromMillisecondsSinceEpoch(timeStamp*1000);
  return "${dateTime.year}-${dateTime.month}-${dateTime.day} ${dateTime.hour}:${dateTime.minute}:${dateTime.second}";
}