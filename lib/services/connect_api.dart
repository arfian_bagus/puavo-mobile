import 'dart:async';
import 'dart:convert';
import 'package:puavomobile/services/url_connect.dart' as url_connect;
import 'package:dio/dio.dart';
//import 'package:puavomobile/services/shared_pref.dart' as pref;
import 'package:dio/dio.dart';
import 'dart:io';
import 'package:path/path.dart';

Future<bool> loginDo({
  String email,
  String password
}) async {

  try {
    Map<String, dynamic> dataParams = {
      "Username": email,
      "Password": password
    };

    var dio = await url_connect.dio;
    Response<String> response =
    await dio.post("api/loginSales", data: dataParams );
    var json = jsonDecode(response.data);
    // print("=========");
    // print(json["success"]);
    if (json["success"] == true) {
//      pref.addNama(json["message"][0]["NamaSales"]);
//      pref.addUsername(json["message"][0]["Username"]);
//      pref.addIdSales(int.parse(json["message"][0]["SalesId"]));
      return true;
    } else {
      throw json["message"];
    }
  } on DioError catch (e) {
    if (e.response != null) {
      var json = jsonDecode(e.response.data);
      throw json["message"];
    } else {
      throw e.message;
    }
  }
}