import 'dart:async';
import 'dart:convert';
import 'package:puavomobile/services/url_connect.dart' as url_connect;
import 'package:dio/dio.dart';
import 'package:puavomobile/services/shared_pref.dart' as pref;
import 'package:puavomobile/models/schoolmodel.dart';
import 'package:puavomobile/style/error_dialog.dart';
import 'package:puavomobile/services/globals.dart' as globals;

Future<bool> loginDo({
  String username,
  String password,
  Function refreshData
}) async {

  try {
    pref.addUsername(username);
    pref.addPassword(password);

    var dio = await url_connect.dio;
    Response<String> response =
    await dio.get("v3/auth");
    var json = jsonDecode(response.data);

//    print("======= id school ======");
//    print(json["id"]);
//    print(json["first_name"] + " " + json["last_name"]);
    var schoolList = SchoolAuthModel.jsonToList(json["schools"]);
    if (schoolList.length > 0)
      pref.addIdSchool(schoolList[0].id);
    else
      throw "school not found";

    pref.addNama(json["first_name"].toString() + " " + json["last_name"].toString());
    pref.addTimeZone(json["timezone"]);
    return true;
  } on DioError catch (e) {
    if (e.response != null) {
      var json = jsonDecode(e.response.data);
      throw json["error"]["code"];
    } else {
      url_connect.internetError(e: e, loading: false, refreshData: refreshData);
      throw "error connection";
    }
  }
}

Future<bool> changePass({
  String typeChange,
  String username,
  String password,
  String retype,
  Function refreshData
}) async {
  try {
//    FormData parameters = FormData();

    var domain = await pref.getDomain();
    var username_actor = await pref.getUsername();
    var password_actor = await pref.getPassword();

//    parameters.add('actor_username', username_actor);
//    parameters.add('actor_password', password_actor);
//    parameters.add('host', domain+url_connect.base_url);
//    parameters.add('target_user_username', username);
//    parameters.add('target_user_password', password);

    Map<String, dynamic> jsonMap = {
      'actor_username': username_actor,
      'actor_password': password_actor,
      'host': "ldap1"+url_connect.base_url,
      'target_user_username': username,
      'target_user_password': password,
      'mode': "all"
    };
    var dio = await url_connect.dio;
    Response<String> response =
    await dio.put("v3/users/password", data: jsonMap);
    var json = jsonDecode(response.data);
    print("======= change password ======");
    print(json);
    if (json['exit_status'] == 0){
      if (typeChange == "self"){
        pref.deletePassword();
        pref.addPassword(password);
      }
      return true;
    } else
      return false;
  } on DioError catch (e) {
    if (e.response != null) {
      var json = jsonDecode(e.response.data);
      throw json["error"]["code"];
    } else {
      url_connect.internetError(e: e, loading: false, refreshData: refreshData);
      throw "error connection";
    }
  }
}