import 'package:flutter/cupertino.dart';
import 'package:puavomobile/models/schoolmodel.dart';

String urlactive = "https://nola.opinsys.fi/users/login";
bool statusLogin = false;
String idUrl = "";
BuildContext _contextData;
SchoolModel schoolData = new SchoolModel();

void setContext(BuildContext context){
  _contextData = context;
}

BuildContext getLastContext(){
  try{
    return _contextData;
  }catch(e){
    return null;
  }
}