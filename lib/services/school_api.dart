import 'dart:async';
import 'dart:convert';
import 'package:puavomobile/models/schoolmodel.dart';
import 'package:puavomobile/services/url_connect.dart' as url_connect;
import 'package:dio/dio.dart';

Future<List<SchoolModel>> getSchool({
  Function refreshData
}) async {
  try {
    var dio = await url_connect.dio;
    Response<String> response =
    await dio.get("v3/schools");
    return SchoolModel.jsonToList(jsonDecode(response.data));
  } on DioError catch (e) {
    if (e.response != null) {
      var json = jsonDecode(e.response.data);
      throw json["error"]["code"];
    } else {
      url_connect.internetError(e: e, loading: false, refreshData: refreshData);
      throw "error connection";
    }
  }
}

Future<List<SchoolAuthModel>> authUser({
  Function refreshData
}) async {

  try {
    var dio = await url_connect.dio;
    Response<String> response =
    await dio.get("v3/auth");
    var json = jsonDecode(response.data);
    
    var schoolList = SchoolAuthModel.jsonToList(json["schools"]);
    if (schoolList.length > 0)
      return schoolList;
    else
      throw "school not found";
  } on DioError catch (e) {
    if (e.response != null) {
      var json = jsonDecode(e.response.data);
      throw json["error"]["code"];
    } else {
      url_connect.internetError(e: e, loading: false, refreshData: refreshData);
      throw "error connection";
    }
  }
}