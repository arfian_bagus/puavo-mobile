import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:puavomobile/services/shared_pref.dart' as pref;
import 'package:puavomobile/style/error_dialog.dart';
import 'package:puavomobile/services/globals.dart' as globals;

var base_url = ".opinsys.fi";
//var base_img = "https://";

Dio dioInstance;

createInstance() {
  var options = BaseOptions(
    baseUrl: "$base_url/",
    connectTimeout: 60000,
    receiveTimeout: 60000,
    headers: {
      "Accept": "application/json",
      // "Content-Type": "application/x-www-form-urlencoded"
    },
  );
  dioInstance = new Dio(options);
  dioInstance.interceptors.add(LogInterceptor(
    error: true,
    responseBody: true,
    request: true,
    requestBody: true,
    requestHeader: true,
  ));
}

Future<Dio> get dio async {
  if (dioInstance == null) {
    createInstance();
  }
  var domain = await pref.getDomain();
  var username = await pref.getUsername();
  var password = await pref.getPassword();

  if (username != null && password != null) {
    String basicAuth =
        'Basic ' + base64Encode(utf8.encode('$username:$password'));
    dioInstance.options.headers['Authorization'] = basicAuth;
  }else{
    dioInstance.options.headers['Authorization'] = "no token";
  }
  dioInstance.options.baseUrl = "https://$domain$base_url/";

  return dioInstance;
}

internetError({DioError e, bool loading = false, Function refreshData }) {
  var check = false;
  if (e.type == DioErrorType.CONNECT_TIMEOUT)
    check = true;
  else if (e.type == DioErrorType.RECEIVE_TIMEOUT)
    check = true;
  else if (e.type == DioErrorType.RESPONSE)
    check = true;
  else if (e.type == DioErrorType.DEFAULT)
    check = true;

  if (check)
    new ErrorDialog(globals.getLastContext()).setMessage("").errorConnection();

}